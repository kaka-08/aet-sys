import React from 'react'
import ETable from 'components/ETable';
import EFilterForm from 'components/EFilterForm';
import { queryEnvService } from 'service'
import styles from './index.module.less';


export default class ListDemo extends React.PureComponent{

    state={}

    handleClick=()=>{
        ETable.refresh();
    }

    handleFilter=(params)=>{
        this.params = {...params};
        ETable.refresh(this.params);
    }


    doRequestList=()=>{
        return queryEnvService()
    }

    handleChange=(e)=>{
    }

    render(){
        const columns = [{
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name0',
            key: 'name0',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name',
            key: 'name',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name1',
            key: 'name1',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name2',
            key: 'name2',
            align: 'center'
        },
        {
            title: '英文名称',
            dataIndex: 'en_name',
            key: 'en_name',
            align: 'center'
        }];
        return(
            <>
                <div className={styles.okok}>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "name",
                                label: "名称",
                                type: "input",
                                placeholder: "填写名称",
                                onChange: this.handleChange
                            },
                            {
                                field: "password",
                                label: "密码",
                                type: "input",
                                placeholder: "填写密码"
                            },
                            {
                                field: "city",
                                label: "下拉选项",
                                type: "select",
                                placeholder: "下拉选项",
                                options:[
                                    { id: "", name:"请选择" },
                                    { id: "1", name:"北京" },
                                    { id: "2", name:"上海" },
                                    { id: "3", name:"深圳" },
                                    { id: "4", name:"太原" }
                                ]
                            },
                            {
                                field: "age",
                                label: "年纪",
                                type: "input",
                                placeholder: "输入年纪"
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <ETable
                        tip="查询表格"
                        size={"middle"}
                        columns={columns}
                        params={this.params}
                        listService={this.doRequestList} 
                    />
                </div>
            </>
        )
    }
}