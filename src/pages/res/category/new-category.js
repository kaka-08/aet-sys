import React from 'react';
import { Form, Input, Row, Col, Button } from 'antd';
import { queryCategoriesByIdService } from 'service'
import './new-category.less';
import EHorizontal2SelectItems from 'components/EHorizontal2SelectItems';

const kvMaps = {
    1: '一级',
    2: '二级',
    3: '三级',
};
class NewCategory extends React.PureComponent{

    state = {
        options: [],
        subOptions: [],
        values: { pid: '', id: '' },
        level: 1
    }

    componentDidMount(){
        const { info } = this.props;
        this.setState({
            level: info.level || 1,
            values: { pid: info.pid || '', id: info.id || '' }
        })
        this.initLevelValue(1);
        queryCategoriesByIdService(0).then(res=>{
            this.setState({
                options: res
            })
        })
        queryCategoriesByIdService(info.pid).then(res=>{
            this.setState({
                subOptions: res
            })
        })
    }

    resetSelects=()=>{
        this.initLevelValue(1);
        this.setState({
            values: { pid: '', id: '' },
            level: 1
        })
    }

    initLevelValue=(level=1)=>{
        const { setFieldsValue } = this.props.form;
        setFieldsValue({level});
    }

    handleChange = ({pid,id})=>{
        const { values } = this.state;
        if(pid){
            this.initLevelValue(2);
            queryCategoriesByIdService(pid).then(res=>{
                this.setState({
                    subOptions: res,
                    values: Object.assign(values,{pid,id:''}),
                    level: 2
                })
            })
        }

        if(id){
            this.initLevelValue(3);
            this.setState({
                values: Object.assign(values,{id}),
                level: 3
            })
        }
    }
    
    render(){
        const { options,subOptions,values,level } = this.state;
        const { getFieldDecorator } = this.props.form;
        const { info } = this.props;
        const formItemLayout = {
            labelCol: {
              sm: { span: 6 },
            },
            wrapperCol: {
              sm: { span: 14 },
            },
          };
        return(
            <Form {...formItemLayout}>
                <Form.Item label="品类名称">
                    {getFieldDecorator('category_name', {  
                        initialValue: info.category_name || '',
                        rules: [{ required: true,message:"请填写品类名称" }],
                    })(<Input placeholder="请填写品类名称" />)}
                </Form.Item>
                <Form.Item label="上级品类">
                    <Row>
                        <Col span={22}>
                        {   
                            getFieldDecorator('parent_category', {  
                                initialValue: values,
                                // rules: [{ validator: this.checkSelect },{ required: false }],
                            })(<EHorizontal2SelectItems 
                                onChange={this.handleChange}
                                options={options}
                                values={values}
                                subOptions={subOptions}
                                placeholder="请选择上级品类" />)
                            }
                        </Col>
                        <Col span={2}>
                            <Button type="link" onClick={this.resetSelects}>重置</Button>
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item label="品类层级">
                    {getFieldDecorator('level', {})(<span>{kvMaps[level]}</span>)}
                    <span className="ant-form-text">由您选择的品类级别生成</span>
                </Form.Item>
                <Form.Item label="显示排序"><span>1</span><span className="ant-form-text">预留字段，后期有必要会加此功能</span></Form.Item>
            </Form>
        )
    }
}

export default Form.create()(NewCategory);