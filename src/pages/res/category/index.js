import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import { categoriesService,categorieAddService,categorieUpdateService } from 'service'
import { Badge,Modal,Button,Icon } from 'antd';
import NewCategory from './new-category';


class Category extends React.PureComponent{

    state={
        visible: false,
        info: {}
    }

    params={}

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    reload=()=>{
        EDragTable.refresh(this.params);
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return categoriesService(finalParams)
    }

    showModal = () => {
        this.setState({
          visible: true,
        });
    };

    handleEdit =(record)=>{
        this.setState({
            visible: true,
            info: record
        });
    }
    
    handleOk = e => {
        const { info } = this.state;
        this.newCategoryForm.props.form.validateFieldsAndScroll((err,values)=>{
            if(!err){
                if(values.level === 1){
                    values.pid = 0;
                }else{
                    values.pid = values.parent_category.pid || values.parent_category.id;
                    delete values.parent_category;
                }
                if(info.id){
                    values.id = info.id;  
                    categorieUpdateService(values).then(res=>{
                        if(res){
                            this.setState({
                                visible: false  
                            },()=>{
                                this.reload()
                            })
                        }
                    })
                }else{
                    categorieAddService(values).then(res=>{
                        if(res){
                            this.setState({
                                visible: false  
                            },()=>{
                                this.reload()
                            })
                        }
                    })
                }
            }
        })
    };

    handleCancel = e => {
        this.setState({
            visible: false,
            info: {}
        });
    };

    render(){
        const columns = [
        {
            title: '上级品类ID',
            dataIndex: 'pid',
            key: 'pid',
            align: 'center',
            width: 80
        },
        {
            title: '名称',
            dataIndex: 'category_name',
            key: 'category_name',
            align: 'center'
        },
        {
            title: '级别',
            dataIndex: 'level',
            key: 'level',
            align: 'center',
            render: t => {
                let kvMaps = {
                    1: '一级',
                    2: '二级',
                    3: '三级',
                };
                return <span>{kvMaps[t]}</span>
            }
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "正常",
                    1: "禁用",
                    2: "已删除"
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        },
        {
            title: '更新时间',
            dataIndex: 'update_time',
            key: 'update_time',
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operator',
            key: 'operator',
            align: 'center',
            render: (t,r)=><Button onClick={(e)=>this.handleEdit(r)} type="link">编辑</Button>
        }];
        const { visible,info } = this.state;
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入商标名称/商标码查询"
                            },
                            {
                                field: "status",
                                label: "状态",
                                type: "select",
                                placeholder: "请选择商标状态",
                                initialValue: '-1',
                                options:[
                                    { id: "-1", name:"全部" },
                                    { id: "0", name:"正常" },
                                    { id: "1", name:"禁用" },
                                    { id: "2", name:"已删除" }
                                ]
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <div className="operate-area">
                        <Button type="primary" onClick={this.showModal} >新增</Button>
                        <Icon type="reload" onClick={this.reload} className="reload-btn" />
                    </div>
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="品类列表"
                      size={"middle"}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                     <Modal
                        title={JSON.stringify(info) !== "{}" ? "编辑品类" : "创建品类"}
                        visible={visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        width={720}
                        destroyOnClose={true}
                        >
                        <NewCategory info={info} wrappedComponentRef={(form) => this.newCategoryForm = form}/>
                    </Modal>
                </div>
            </>
        )
    }
}

export default Category