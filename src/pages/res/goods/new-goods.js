import React from 'react';
import { Form,Input, Select, InputNumber,Switch,Icon,Upload,Modal,message } from 'antd';
import './new-goods.less';
import { queryCategorysService,querySklTplsService } from 'service';

const Option = Select.Option;
const { TextArea } = Input;

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('图片格式不正确');
      return false
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
}

class BaseGoods extends React.PureComponent{

    state = {
        skuTpls: [],
        categories: [],
        previewVisible: false,
        previewImage: "",
        mainImage:[],
        mainLoading: false,
        subImages:[],
        loading: false
    }

    generateImgs=(urls='')=>{
        if(urls){
            let temp = urls.split(',');
            let result = temp.map(item=>{
                return {
                    uid: Math.random().toFixed(3),
                    name: `image-${Math.random().toFixed(3)}.png`,
                    status: 'done',
                    url: item
                }
            })
            return result
        }else{
            return ''
        }
    }

    componentDidMount(){
        const { info } = this.props;
        this.setState({
            mainImage: this.generateImgs(info.main_image),
            subImages: this.generateImgs(info.sub_images),
        })
        Promise.all([
            queryCategorysService(),
            querySklTplsService()])
        .then(res=>{
            this.setState({
                categories: res[0],
                skuTpls: res[1]
            })
        })
    }

    handlePreview = file => {
        this.setState({
          previewImage: file.url,
          previewVisible: true,
        });
    };


    mainImageUpload =({file,fileList})=>{
        if (file.status === 'uploading') {
            this.setState({
                mainLoading: true,
            })
        }else if (file.status === 'done') {
            this.setState({
                mainLoading: false
            })
        }else if (file.status === 'error') {
            this.setState({
                mainLoading: false
            })
            message.error(`${file.name} 上传失败.`);
        }
        this.setState({ mainImage: [...fileList] });
    }

    subImagesUpload =({file,fileList})=>{
        if (file.status === 'uploading') {
            this.setState({
                loading: true
            })
        }
        if (file.status === 'done') {
            this.setState({
                loading: false
            })
        }else if (file.status === 'error') {
            this.setState({
                loading: false
            })
            message.error(`${file.name} 上传失败.`);
        }
        this.setState({ subImages: [...fileList] });
    }

    handleCancel =()=>{
        this.setState({
            previewImage: "",
            previewVisible: false,
        }); 
    }

    saveName=(v,option,key)=>{
        const { setFieldsValue } = this.props.form;
        let name = option.props.children;
        setFieldsValue({[key]:name})
    }

    render(){
        const props = {
            name: 'file',
            action: '/v1/common/file/upload',
            headers: {
              authorization: 'authorization-text',
            },
        };
        const { info } = this.props;
        const { getFieldDecorator } = this.props.form;
        const { categories,skuTpls,mainImage,mainLoading,subImages,previewVisible,previewImage } = this.state;
        const formItemLayout = {
            labelCol: {
              sm: { span: 6 },
            },
            wrapperCol: {
              sm: { span: 16 }
            },
          };    
        return(
            <div className="new-goods-container">
                <Form {...formItemLayout}>
                    <Form.Item label="商品名" extra={<span>长度为5-16位,名称要更容易理解，例如 <b className="tip">康师傅红茶、脉动、康师傅绿茶</b> 之类</span>}>
                        {getFieldDecorator('name', {
                            initialValue: info.name,
                            rules: [ 
                                { required: true, message: '请输入商品名' },
                            ],
                        })(<Input />)}
                    </Form.Item>
                    <Form.Item label="商品模版" extra={<span>模板不满足您的条件？去 <a> 新增 </a></span>}>
                        {getFieldDecorator('sku_tpl_id', {
                            initialValue: info.sku_tpl_id,
                            rules: [
                            { required: true,message: '请选择模板'},
                            ],
                        })(<Select className="select-box" onSelect={(v,option)=>this.saveName(v,option,'sku_tpl_name')}>
                            {
                                skuTpls.map(item=><Option key={item.id} value={item.id}>{item.name}</Option>)
                            }
                        </Select>)}
                        <a className="ant-form-text" icon="reload"> <Icon type="reload" /> 刷新</a>
                    </Form.Item>
                    <Form.Item label="商品模版名称" className="form-item-hide">{getFieldDecorator('sku_tpl_name', {initialValue: info.sku_tpl_name})(<span />)}</Form.Item>
                    <Form.Item label="品类" extra={<span>品类不全？去<a> 新增 </a></span>}>
                        {getFieldDecorator('category_id', {
                            initialValue: info.category_id,
                            rules: [
                            { required: true,message: '请选择品类'},
                            ],
                        })(<Select className="select-box" onSelect={(v,option)=>this.saveName(v,option,'category_name')}>
                            {
                                categories.map(item=><Option key={item.id} value={item.id}>{item.name}</Option>)
                            }
                        </Select>)}
                        <a className="ant-form-text" icon="reload"> <Icon type="reload" /> 刷新</a>
                    </Form.Item>
                    <Form.Item label="品类名称" className="form-item-hide">{getFieldDecorator('category_name', {initialValue: info.category_name})(<span />)}</Form.Item>
                    <Form.Item label="售价">
                        {getFieldDecorator('price', {
                            initialValue: info.price,
                            rules: [
                                { required: true,message: '请填写售价'},
                            ],
                        })(<InputNumber />)}
                        <span className="ant-form-text"> 元</span>
                    </Form.Item>
                    <Form.Item label="是否促销">
                        {getFieldDecorator('is_promotion', {
                            valuePropName: "checked",
                            initialValue: !!info.is_promotion,
                            rules: [{ 
                                required: true,message: '请填写售价'
                            }],
                        })(<Switch checkedChildren="是" unCheckedChildren="否" />)}
                    </Form.Item>
                    <Form.Item label="封面" extra="图片会展示为封面，请尽量选择适合宣传的商品图片">
                        {getFieldDecorator('main_image', {
                            rules:[{
                                required: true, message: "请上传封面"
                            }],
                        })(
                            <Upload 
                                {...props} 
                                onPreview={this.handlePreview}
                                beforeUpload={beforeUpload}
                                onChange={this.mainImageUpload} 
                                listType="picture-card"
                                fileList={mainImage}
                                className="avatar-uploader">
                                {
                                    mainImage.length
                                    ? 
                                    <div>
                                        <Icon type={mainLoading ? 'loading' : 'plus'} />
                                        <div className="ant-upload-text">替换封面</div>
                                    </div>
                                    : 
                                    <div>
                                        <Icon type={mainLoading ? 'loading' : 'plus'} />
                                        <div className="ant-upload-text">上传封面</div>
                                    </div>
                                }
                            </Upload>
                        )}
                    </Form.Item>
                    <Form.Item label="商品描述图片">
                        {getFieldDecorator('sub_images', {
                            rules:[
                                {
                                    required: true, message: "请填写商品说明"
                                }],
                        })(
                            <Upload 
                                {...props} 
                                onPreview={this.handlePreview} 
                                onRemove={this.handleRemove} 
                                fileList={subImages}
                                onChange={this.subImagesUpload} 
                                listType="picture-card">
                                {
                                    subImages.length >= 4 
                                    ? 
                                    null
                                    : 
                                    <div>
                                        <Icon type="plus" />
                                        <div className="ant-upload-text">上传详情</div>
                                    </div>
                                }
                            </Upload>
                        )}
                    </Form.Item>
                    <Form.Item label="商品描述信息">
                        {getFieldDecorator('remark', {
                            initialValue: info.remark,
                            rules:[
                                {
                                    required: true, message: "请填写商品说明"
                                }],
                        })(
                            <TextArea
                                placeholder="请填写商品说明"
                                autoSize={{ minRows: 3, maxRows: 5 }}
                            />
                        )}
                    </Form.Item>
                </Form> 
                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                </Modal> 
            </div>
        )
    }
}


export default Form.create()(BaseGoods)