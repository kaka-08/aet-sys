import React from 'react';
import { Descriptions,Card,Table,Badge } from 'antd';
import { goodsDetailService,goodsBatchService } from 'service'
import { taste,unit,purpose,colors,expirationUnit,isPromotion } from 'constants/constants';
import { DetailContainer } from 'components/DetailContainer';
import Loading from 'components/Loading';
import ScanImg from 'components/ScanImg';
import './index.less'

class GoodsInfo extends React.PureComponent{

    state={
        goodsInfo: {},
        skuList:[],
        loading: true
    }

    componentDidMount(){
        let { id,tpl_id } = this.props.match.params;
        Promise.all([
            goodsDetailService(id),
            goodsBatchService(tpl_id)
        ]).then(res=>{
            this.setState({
                goodsInfo: res[0],
                skuList: res[1],
                loading: false
            })
        })
    }

    render(){
        const columns = [{
            title: '批次名称',
            dataIndex: 'sku_name',
            key: 'sku_name',
            align: 'center'
        },
        {
            title: '入库数量',
            dataIndex: 'stock',
            key: 'stock',
            align: 'center'
        },
        {
            title: '生产日期',
            dataIndex: 'production_date',
            key: 'production_date',
            render: t=><i style={{fontSize: 12,fontWeight: 600}}>{t}</i>,
            align: 'center'
        },
        {
            title: '进价（单价）',
            dataIndex: 'purchasing_price',
            key: 'purchasing_price',
            render: t=> "¥ "+(t/100).toFixed(2),
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            render: t=>{ return { 0:<Badge color="#87d068" text="正常" />,1:<Badge color="#f50" text="回收" /> }[t] },
            align: 'center'
        },
        {
            title: '入库时间',
            dataIndex: 'create_time',
            key: 'create_time',
            align: 'center'
        },
        {
            title: '最后变更时间',
            dataIndex: 'update_time',
            key: 'update_time',
            align: 'center'
        }];
        const { goodsInfo,loading,skuList } = this.state;
        const { sku_tpl = {},brand = {} } = goodsInfo;
        return(
            <div className="main">
                {
                    loading 
                    ?
                    <Loading />
                    :
                    <>
                    <Card bordered={false}>
                        <Descriptions title="商品基础规格" bordered>
                            <Descriptions.Item label="模版code">{sku_tpl.code}</Descriptions.Item>
                            <Descriptions.Item label="规格">{sku_tpl.capacity + ' ' + unit.get(sku_tpl.unit)}</Descriptions.Item>
                            <Descriptions.Item label="用途">{purpose.get(sku_tpl.purpose)}</Descriptions.Item>
                            <Descriptions.Item label="口味">{taste.get(sku_tpl.taste)}</Descriptions.Item>
                            <Descriptions.Item label="颜色">{colors.get(sku_tpl.color)}</Descriptions.Item>
                            <Descriptions.Item label="保质期">{sku_tpl.expiration_date + ' '+ expirationUnit.get(sku_tpl.expiration_unit)}</Descriptions.Item>
                        </Descriptions>
                    </Card>
                    <Card bordered={false}>
                        <Descriptions title="商品展示信息" bordered>
                            <Descriptions.Item label="名称">{goodsInfo.name}</Descriptions.Item>
                            <Descriptions.Item label="所属品牌"><img src={brand.brand_logo} style={{width: 75,height: 30}}/> {brand.brand_name}</Descriptions.Item>
                            <Descriptions.Item label="库存（件）">{goodsInfo.stock}</Descriptions.Item>
                            <Descriptions.Item label="单价（元）" Amount>¥ {(goodsInfo.price/100).toFixed(2)}</Descriptions.Item>
                            <Descriptions.Item label="分类">{goodsInfo.category_name}</Descriptions.Item>
                            <Descriptions.Item label="是否促销">{isPromotion.get(goodsInfo.is_promotion)}</Descriptions.Item>
                            <Descriptions.Item label="封面（点击图片预览）" span={3}><ScanImg src={goodsInfo.main_image} style={{width: 150, height: 150}}/></Descriptions.Item>
                            <Descriptions.Item label="详情图片（点击图片预览）" span={3}>
                                {
                                    goodsInfo.sub_images && goodsInfo.sub_images.split(",").map((item,index)=><ScanImg key={index} src={item} style={{width: 150,height: 150, marginRight: 20}}/>)
                                }
                            </Descriptions.Item>
                            <Descriptions.Item label="描述" span={3} Info>
                                {goodsInfo.remark}
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>
                    <Card bordered={false} title="批次列表">
                        <Table
                            rowKey="id"
                            bordered
                            size={"middle"}
                            rowSelection={null}
                            columns={columns}
                            pagination={false}
                            dataSource={skuList}
                        />
                    </Card>
                </>
                }
            </div>
        )
    }
}

export default DetailContainer(GoodsInfo);