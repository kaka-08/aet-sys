/* eslint-disable eqeqeq */
import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import { goodsService,goodsAddService } from 'service'
import { Badge,Button, Icon,Modal } from 'antd';
import Utils from 'utils/Util';
import NewGoods from './new-goods';
import ScanImg from 'components/ScanImg';
import './index.less'

class Goods extends React.PureComponent{

    state={
        visible: false,
        goodsInfo: {}
    }

    params={}

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    reload=()=>{
        EDragTable.refresh(this.params);
    }

    goToCreate=()=>{
        this.props.history.push('/res/sku/tpl-edit/0')
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return goodsService(finalParams)
    }
    
    handleOk = e => {
        this.goodsForm.props.form.validateFieldsAndScroll((err,values)=>{
            if(!err){
                values.main_image = values.main_image.file.response.data;
                values.sub_images = values.sub_images.fileList.map(item=>item.response.data).join();
                values.is_promotion = values.is_promotion ? 1 : 0;
                goodsAddService(values).then(res=>{
                    this.setState({
                        visible: false  
                    },()=>{
                        this.reload()
                    })
                })
            }
        })
    };

    handleCancel = e => {
        this.setState({
            visible: false,
            goodsInfo: {}
        });
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    baseEdit=(record)=>{
        this.setState({
            visible: true,
            goodsInfo: record
        })
    }

    handleDetail=(id,tpl)=>{
        Utils.goBlankPage(`/#/res/goods-info/${id}/${tpl}`)
    }

    render(){
        const columns = [
        {
            title: '规格参数模版',
            dataIndex: 'sku_tpl_name',
            key: 'sku_tpl_name',
            align: 'center'
        },
        {
            title: '商品名称',
            dataIndex: 'name',
            key: 'name',
            align: 'center'
        },
        {
            title: '类别',
            dataIndex: 'category_name',
            key: 'category_name',
            align: 'center'
        },
        {
            title: '封面',
            dataIndex: 'main_image',
            key: 'main_image',
            render: t=><ScanImg src={t} style={{width: 50,height: 70}}/>,
            align: 'center'
        },
        {
            title: '单价（元）',
            dataIndex: 'price',
            key: 'price',
            render: t=>"¥ " + (t / 100).toFixed(2),
            align: 'center'
        },
        {
            title: '库存（箱）',
            dataIndex: 'stock',
            key: 'stock',
            align: 'center'
        },
        {
            title: '是否促销',
            dataIndex: 'is_promotion',
            key: 'is_promotion',
            render: t=>{
                return { 0:'否',1:'是' }[t]
            },
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "正常",
                    1: "废弃",
                    2: "已删除"
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        },
        {
            title: '操作',
            dataIndex: 'operate',
            key: 'operate',
            render:(t,r)=>{
                return (
                    <>
                        <Button type="link" onClick={e=>this.baseEdit(r)}> 编辑 </Button>
                        <Button type="link" onClick={(e)=>this.handleDetail(r.id,r.sku_tpl_id)}> 详情 </Button>
                    </>
                )
            },
            align: 'center'
        }];
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入模版ID/商品名称/分类/ID查看",
                                colSpan: 8
                            },
                            {
                                field: "status",
                                label: "状态",
                                type: "select",
                                placeholder: "请选择模版状态",
                                initialValue: '-1',
                                colSpan: 4,
                                options:[
                                    { id: "-1", name:"全部" },
                                    { id: "0", name:"正常" },
                                    { id: "1", name:"废弃" },
                                    { id: "2", name:"已删除" }
                                ]
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <div className="operate-area">
                        <Button type="primary" onClick={this.showModal} >新增</Button>
                        <Icon type="reload" onClick={this.reload} className="reload-btn" />
                    </div>
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="商品列表"
                      size={"middle"}
                      rowSelection={null}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                    <Modal
                        title="添加商品"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        width={720}
                        destroyOnClose={true}
                        >
                        <NewGoods info={this.state.goodsInfo} wrappedComponentRef={(form) => this.goodsForm = form}/>
                    </Modal>
                </div>
            </>
        )
    }
}

export default Goods