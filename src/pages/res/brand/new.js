import React from 'react';
import { Form, Input,Upload,Icon,message } from 'antd';


class NewBrand extends React.PureComponent{

    state = {
        brandLogo: '',
        loading: false
    }

    componentDidMount(){
        const { info } = this.props;
        const brandLogo = info.brand_logo ? info.brand_logo : '';
        this.props.form.setFieldsValue({'brand_logo': brandLogo});
        this.setState({
            brandLogo
        })
    }

    handleUploadImgs=({file})=>{
        if (file.status === 'uploading') {
            this.setState({
                loading: true
            })
        }
        if (file.status === 'done') {
            this.setState({
                brandLogo: file.response.data,
                loading: false
            })
        } else if (file.status === 'error') {
            this.setState({
                loading: false
            })
            message.error(`${file.name} 上传失败.`);
        }
    }

    render(){
        const { info } = this.props;
        const { brandLogo,loading } = this.state;
        const props = {
            name: 'file',
            action: '/v1/common/file/upload',
            headers: {
              authorization: 'authorization-text',
            },
          };
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
              sm: { span: 6 },
            },
            wrapperCol: {
              sm: { span: 14 },
            },
          };
        return(
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item label="商标名称">
                    {getFieldDecorator('brand_name', {  
                        initialValue: info.brand_name || "",
                        rules: [{ required: true,message:"请填写商标名称" }],
                    })(<Input placeholder="请填写商标名称" />)}
                </Form.Item>
                <Form.Item label="商标logo">
                    {getFieldDecorator('brand_logo', {  
                        rules: [{ required: true,message:"请填写商标图标" }],
                    })(
                        <Upload 
                            {...props} 
                            onPreview={this.handlePreview} 
                            onChange={info=>this.handleUploadImgs(info)} 
                            showUploadList={false} 
                            listType="picture-card"
                            className="avatar-uploader">
                            {
                                brandLogo 
                                ? 
                                <img src={brandLogo} alt="商标图标" style={{ width: '100%' }} />
                                : 
                                <div>
                                    <Icon type={loading ? 'loading' : 'plus'} />
                                    <div className="ant-upload-text">上传图标</div>
                                </div>
                            }
                        </Upload>
                    )}
                </Form.Item>
            </Form>
        )
    }
}

export default Form.create()(NewBrand)