import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import ScanImg from 'components/ScanImg';
import { brandsService,brandAddService,brandUpdateService } from 'service'
import { Badge,Button,Icon,Modal } from 'antd';
import NewBrand from './new';


class Brand extends React.PureComponent{

    state={
        visible: false,
        brandInfo: {}
    }

    params={}

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    reload=()=>{
        EDragTable.refresh(this.params);
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return brandsService(finalParams)
    }

    addBrand =()=>{
        this.setState({
            visible: true
        })
    }

    handleCancel =()=>{
        this.setState({
            visible: false,
            brandInfo: {}
        })
    }

    handleOk = e => {
        const { brandInfo } = this.state;
        this.newBrandForm.props.form.validateFieldsAndScroll((err,values)=>{
            if(!err){
                if(values.brand_logo.file){
                    values.brand_logo = values.brand_logo.file.response.data;
                }
                if(JSON.stringify(brandInfo) !== "{}"){
                    values.id = brandInfo.id;
                    brandUpdateService(values).then(res=>{
                        if(res){
                            this.setState({
                                visible: false  
                            },()=>{
                                this.reload()
                            })
                        }
                    })
                }else{
                    brandAddService(values).then(res=>{
                        if(res){
                            this.setState({
                                visible: false  
                            },()=>{
                                this.reload()
                            })
                        }
                    })
                }
            }
        })
    };

    editBrand=(record)=>{
        this.setState({
            brandInfo: record,
            visible: true
        })
    }

    render(){
        const { visible,brandInfo } = this.state;
        const columns = [{
            title: '商标码',
            dataIndex: 'brand_code',
            key: 'brand_code',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'brand_name',
            key: 'brand_name',
            align: 'center'
        },
        {
            title: 'logo',
            dataIndex: 'brand_logo',
            key: 'brand_logo',
            align: 'center',
            render: text=><ScanImg src={text} />
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "正常",
                    1: "禁用",
                    2: "已删除"
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        },
        {
            title: '创建人',
            dataIndex: 'create_user',
            key: 'create_user',
            align: 'center',
            render: t => t || '--'
        },
        {
            title: '更新人',
            dataIndex: 'update_user',
            key: 'update_user',
            align: 'center',
            render: t => t || '--'
        },
        {
            title: '创建时间',
            dataIndex: 'create_time',
            key: 'create_time',
            align: 'center'
        },
        {
            title: '更新时间',
            dataIndex: 'update_time',
            key: 'update_time',
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operate',
            key: 'operate',
            align: 'center',
            render: (t,r) => {
                return (
                    <Button type="link" onClick={()=>this.editBrand(r)}>编辑</Button>
                )
            }
        }];
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入商标名称/商标码查询"
                            },
                            {
                                field: "status",
                                label: "状态",
                                type: "select",
                                placeholder: "请选择商标状态",
                                initialValue: '-1',
                                options:[
                                    { id: "-1", name:"全部" },
                                    { id: "0", name:"正常" },
                                    { id: "1", name:"禁用" },
                                    { id: "2", name:"已删除" }
                                ]
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                <div className="operate-area">
                        <Button type="primary" onClick={this.addBrand}>添加商标</Button>
                        <Icon type="reload" onClick={this.reload} className="reload-btn" />
                    </div>
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="商标列表"
                      size={"middle"}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                    <Modal
                        title={ JSON.stringify(brandInfo) === "{}" ? "添加商标" : "编辑商标" }
                        visible={visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        width={500}
                        destroyOnClose={true}
                        >
                        <NewBrand info={brandInfo} wrappedComponentRef={(form) => this.newBrandForm = form}/>
                    </Modal>
                </div>
            </>
        )
    }
}

export default Brand
