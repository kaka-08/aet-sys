import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import { skusService,skuInfoService } from 'service'
import { Badge, Button, Alert,Modal,Descriptions,Card } from 'antd';
import { taste,unit,purpose,colors,expirationUnit } from 'constants/constants';
import './index.less';

class Sku extends React.PureComponent{

    state={
        visible: false,
        detailInfo: {
            sku_tpl: {},
            brand: {}
        }
    }

    params={}

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return skusService(finalParams)
    }

    // 查看sku详情
    defailInfo=(r)=>{
        skuInfoService(r.id).then(res=>{
            this.setState({
                visible: true,
                detailInfo: res
            })
        })
    }

    handleCancel=()=>{
        this.setState({
            visible: false
        })
    }

    render(){
        const columns = [
        {
            title: '名称',
            dataIndex: 'sku_name',
            key: 'sku_name',
            align: 'center'
        },
        {
            title: '批次数量（ 件 ）',
            dataIndex: 'stock',
            key: 'stock',
            align: 'center'
        },
        {
            title: '生产日期',
            dataIndex: 'production_date',
            key: 'production_date',
            align: 'center'
        },
        {
            title: '单价（元）',
            dataIndex: 'purchasing_price',
            key: 'purchasing_price',
            render: t=> "¥ " + t,
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "正常",
                    1: "已回收",
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        },
        {
            title: '生成时间',
            dataIndex: 'create_time',
            key: 'create_time',
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operate',
            key: 'operate',
            render:(t,r)=>{
                return (
                    <>
                        <Button type="link" onClick={(e)=>this.defailInfo(r)}> 详情 </Button>
                    </>
                )
            },
            align: 'center'
        }];
        const { detailInfo } = this.state;
        const { sku_tpl,brand } = detailInfo;
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入ID"
                            },
                            {
                                field: "status",
                                label: "状态",
                                type: "select",
                                placeholder: "请选择SKU状态",
                                initialValue: '-1',
                                options:[
                                    { id: "-1", name:"全部" },
                                    { id: "0", name:"正常" },
                                    { id: "1", name:"已回收" }
                                ]
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="SKU列表"
                      size={"middle"}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                     <Modal
                        title="sku 信息"
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        footer={null}
                        width={780}
                        >
                        <>
                            <Card bordered={false}>
                                <Descriptions title="产品基础规格" bordered>
                                    <Descriptions.Item label="模版code">{sku_tpl.code}</Descriptions.Item>
                                    <Descriptions.Item label="所属品牌">{sku_tpl.brand_name}</Descriptions.Item>
                                    <Descriptions.Item label="规格">{sku_tpl.capacity +unit.get(sku_tpl.unit)}</Descriptions.Item>
                                    <Descriptions.Item label="用途">{purpose.get(sku_tpl.purpose)}</Descriptions.Item>
                                    <Descriptions.Item label="口味">{taste.get(sku_tpl.taste)}</Descriptions.Item>
                                    <Descriptions.Item label="颜色">{colors.get(sku_tpl.color)}</Descriptions.Item>
                                    <Descriptions.Item label="级别">{sku_tpl.level}</Descriptions.Item>
                                    <Descriptions.Item label="保质期">{sku_tpl.expiration_date + expirationUnit.get(sku_tpl.expiration_unit)}</Descriptions.Item>
                                </Descriptions>
                            </Card>
                            <Card bordered={false}>
                                <Descriptions title="批次信息" bordered>
                                    <Descriptions.Item label="创建时间">{detailInfo.create_time}</Descriptions.Item>
                                    <Descriptions.Item label="产品数量（件）">{detailInfo.stock}</Descriptions.Item>
                                    <Descriptions.Item label="总价（元）">{detailInfo.purchasing_price}</Descriptions.Item>
                                    <Descriptions.Item label="生产日期">{detailInfo.production_date}</Descriptions.Item>
                                    <Descriptions.Item label="预计到期日期">{detailInfo.production_date}</Descriptions.Item>
                                </Descriptions>
                            </Card>
                        </>
                    </Modal>
                </div>
            </>
        )
    }
}

export default Sku