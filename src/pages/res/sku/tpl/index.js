/* eslint-disable eqeqeq */
import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import { skuTplsService,updateTplStatusService } from 'service'
import { taste,unit,purpose,colors,expirationUnit } from 'constants/constants';
import { Badge,Button,Switch, Icon } from 'antd';
import './index.less'

class SkuTpl extends React.PureComponent{

    state={
        statusSwitchLoading: false
    }

    params={}

    componentDidMount(){
    }

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    reload=()=>{
        EDragTable.refresh(this.params);
    }

    goToCreate=()=>{
        this.props.history.push('/res/sku/tpl-edit/0')
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return skuTplsService(finalParams)
    }

    updateTplStatus=(status,id)=>{
        this.setState({
            statusSwitchLoading: true
        },()=>{
            updateTplStatusService({id,status}).then(res=>{
                setTimeout(()=>{
                    EDragTable.refresh(this.params);
                    this.setState({
                        statusSwitchLoading: false
                    })
                },500)
            })
        })
    }

    render(){
        const columns = [
        {
            title: '名称',
            dataIndex: 'tpl_name',
            key: 'tpl_name',
            align: 'center'
        },
        {
            title: '品牌',
            dataIndex: 'brand_name',
            key: 'brand_name',
            align: 'center'
        },
        {
            title: '容量',
            dataIndex: 'capacity',
            key: 'capacity',
            render: (t,r)=>t + unit.get(r.unit),
            align: 'center'
        },
        {
            title: '用途标签',
            dataIndex: 'purpose',
            key: 'purpose',
            render: t=>purpose.get(t),
            align: 'center'
        },
        {
            title: '口味',
            dataIndex: 'taste',
            key: 'taste',
            render: t=>taste.get(t),
            align: 'center'
        },
        {
            title: '颜色',
            dataIndex: 'color',
            key: 'color',
            render: t=>colors.get(t),
            align: 'center'
        },
        {
            title: '保质期',
            dataIndex: 'expiration_date',
            key: 'expiration_date',
            render: (t,r)=>t + expirationUnit.get(r.expiration_unit),
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "正常",
                    1: "禁用",
                    2: "已删除"
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        },
        {
            title: '禁用 / 启用',
            dataIndex: 'open_close',
            key: 'open_close',
            render:(t,r)=>{
                if(r.status == 1){
                    return<Switch loading={this.state.statusSwitchLoading} checked={false} onChange={()=>this.updateTplStatus(0,r.id)} checkedChildren="禁用" unCheckedChildren="启用" />
                }else if(r.status == 0){
                    return<Switch loading={this.state.statusSwitchLoading} checked={true} onChange={()=>this.updateTplStatus(1,r.id)} checkedChildren="禁用" unCheckedChildren="启用" />
                }else{
                    return<Switch disabled checkedChildren="不可恢复" checked unCheckedChildren="禁用" />
                }
            },
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operate',
            key: 'operate',
            render:(t,r)=>{
                return (
                    <>
                        <Button type="link"> 编辑 </Button>
                        {/* <Switch checkedChildren="启用" unCheckedChildren="禁用" defaultChecked /> */}
                        {/* <Button disabled type="link"> 禁用 </Button> */}
                    </>
                )
            },
            align: 'center'
        }];
        const renderDesc =(record)=>{
            return record.brand_name + "旗下产品 ，【用途】: " + purpose.get(record.purpose)
            + " 【保质期】:" + record.expiration_date + expirationUnit.get(record.expiration_unit) 
            + " 【产品规格】:" + record.capacity + unit.get(record.unit)
        };
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入商标/模板名称/模板code查询",
                                colSpan: 8
                            },
                            {
                                field: "status",
                                label: "状态",
                                type: "select",
                                placeholder: "请选择模版状态",
                                initialValue: '-1',
                                colSpan: 4,
                                options:[
                                    { id: "-1", name:"全部" },
                                    { id: "0", name:"正常" },
                                    { id: "1", name:"禁用" },
                                    { id: "2", name:"已删除" }
                                ]
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <div className="operate-area">
                        <Button type="primary" onClick={this.goToCreate} >新增</Button>
                        <Icon type="reload" onClick={this.reload} className="reload-btn" />
                    </div>
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="模版列表"
                      size={"middle"}
                      expandedRowRender={record => <p style={{ margin: 0 }}>{renderDesc(record)}</p>}
                      rowSelection={null}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                </div>
            </>
        )
    }
}

export default SkuTpl