import React from 'react';
import { Form,Input,Select,Card,Col, Button, Result } from 'antd';
import { taste,unit,purpose,colors,expirationUnit } from 'constants/constants';
import EHorizontal2Items from 'components/EHorizontal2Items';
import { queryBrandsService,addSkuTplService } from 'service'
import './new-tpl.less'

const Option = Select.Option;
class NewTpl extends React.PureComponent{

    state = {
        brands:[],
        brand_name: '',
        result: null
    }

    componentDidMount(){
        queryBrandsService().then(res=>{
            this.setState({
                brands: res
            })
        })
    }

    checkCapacity = (rule, value, callback) => {
        if(!value.data){
            return callback('请输入容量大小，且容量不能为0');
        }else if(!value.unit){
            return callback('请选择容量单位');
        }else{
            return callback();
        }
    };

    checkExpiration =(rule, value, callback)=>{
        if(!value.data){
            return callback('请输入保质期限，且期限不能为0');
        }else if(!value.unit){
            return callback('请选择保质期单位');
        }else{
            return callback();
        }
    }

    handlerSubmit=(e)=>{
        e.preventDefault();
        const { brand_name } = this.state;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                values.brand_name = brand_name;
                values.capacity = values.specs.data;
                values.unit = values.specs.unit;
                values.expiration_date = values.expiration.data;
                values.expiration_unit = values.expiration.unit;
                values.code = Math.floor(Math.random()*10000000000);
                delete values.expiration;
                delete values.specs;
                addSkuTplService(values).then(res=>{
                    setTimeout(()=>{
                        this.setState({
                           result: res 
                        })
                    },500)
                })
            }
        });
    }

    handleSelectBrand=(v,opt)=>{
        this.setState({
            brand_name: opt.props.children
        })
    }

    resetResult=()=>{
        this.setState({
            result: null
        })
    }

    gotoTplList=()=>{
        this.props.history.push('/res/sku/tpl')
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
              sm: { span: 5 },
            },
            wrapperCol: {
              sm: { span: 12 },
            },
          };
        return(
            <div className="main">
                {
                    this.state.result 
                    ?
                    <div className="result">
                        <Result
                            status="success"
                            title="sku模板添加成功"
                            subTitle={`新创建的模版名字为${this.state.result.tpl_name}`}
                            extra={[
                            <Button type="primary" onClick={this.gotoTplList} key="gotolist">去模版列表</Button>,
                            <Button key="continue" onClick={this.resetResult}>继续添加</Button>,
                            ]}
                        />
                    </div>
                    :
                    <Form {...formItemLayout} className="sku-tpl-container">
                        <Card title="基本信息" bordered={false}>
                            <Col span={9}>
                                <Form.Item label="商标">
                                    {
                                        getFieldDecorator('brand_id', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '请选择商标!',
                                                },
                                            ],
                                        })(<Select style={{width: 280}} onSelect={this.handleSelectBrand} placeholder="请填写模板关联商标">
                                            {
                                                this.state.brands.map(item=><Option id={item.id} key={item.id}>{item.brand_name}</Option>)
                                            }
                                        </Select>)
                                    }
                                </Form.Item>
                            </Col>
                            <Col span={5}>
                                <Form.Item label="用途">
                                    {
                                        getFieldDecorator('purpose', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '请选择用途!',
                                                },
                                            ],
                                        })(<Select placeholder="请选择用途">
                                            {
                                                [...purpose].map(item=><Option id={item[0]} key={item[0]}>{item[1]}</Option>)
                                            }
                                        </Select>)
                                    }
                                </Form.Item>
                            </Col>
                            <Col span={5}>
                                <Form.Item label="口味">
                                    {
                                        getFieldDecorator('taste', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '请选择口味!',
                                                },
                                            ],
                                        })(<Select placeholder="请选择口味">
                                            {
                                                [...taste].map(item=><Option id={item[0]} key={item[0]}>{item[1]}</Option>)
                                            }
                                        </Select>)
                                    }
                                </Form.Item>
                            </Col>
                            <Col span={5}>
                                <Form.Item label="颜色">
                                    {
                                        getFieldDecorator('color', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '请选择商标!',
                                                },
                                            ],
                                        })(<Select placeholder="请选择颜色">
                                            {
                                                [...colors].map(item=><Option id={item[0]} key={item[0]}>{item[1]}</Option>)
                                            }
                                        </Select>)
                                    }
                                </Form.Item>
                            </Col>
                        </Card>
                        <Card style={{marginTop: 20,borderTop:'1px solid #e8e8e8'}} title="参数规格" bordered={false}>
                            <Col span={8}>
                                <Form.Item label="容量">
                                {getFieldDecorator('specs', {  
                                    initialValue: { data: '', unit: '' },
                                    rules: [{ validator: this.checkCapacity },{ required: true }],
                                })(<EHorizontal2Items options={[...unit]} placeholder="请填写容量" />)}
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item label="保质期限">
                                {getFieldDecorator('expiration', {
                                    initialValue: { data: '', unit: '' },
                                    rules: [{ validator: this.checkExpiration },{ required: true }],
                                })(<EHorizontal2Items options={[...expirationUnit]} placeholder="请填写保质期限" />)}
                                </Form.Item>
                            </Col>
                        </Card>
                        <Card style={{marginTop: 20,borderTop:'1px solid #e8e8e8'}} title="命名" bordered={false}>
                            <Col span={12}>
                                <Form.Item label="模板名称" extra="模板名称最好和产品名称有关联，这样方便选择">
                                    {
                                        getFieldDecorator('tpl_name', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: '名称不能为空!',
                                                },
                                            ],
                                        })(<Input style={{width: 300}} placeholder="请填写模板名称"/>)
                                    }
                                </Form.Item>
                            </Col>
                            <Col span={12} style={{textAlign: 'right'}}>
                                <Button onClick={this.handlerSubmit} type="primary">创建模版</Button>
                            </Col>
                        </Card>
                    </Form>
                }
            </div>
        )
    }
}


export default Form.create()(NewTpl)