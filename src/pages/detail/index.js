import React from 'react'
import ETable from 'components/ETable';
import EFilterForm from 'components/EFilterForm';
import { queryEnvService } from 'service'
import styles from './index.module.less';
import { DetailContainer } from 'components/DetailContainer';
import './index.module.less'


class DetailDemo extends React.PureComponent{

    state={}

    handleClick=()=>{
        ETable.refresh();
    }

    handleFilter=(params)=>{
        this.params = {...params};
        ETable.refresh(this.params);
    }


    doRequestList=()=>{
        return queryEnvService()
    }

    handleChange=(e)=>{
    }

    render(){
        const columns = [{
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name0',
            key: 'name0',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name',
            key: 'name',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name1',
            key: 'name1',
            align: 'center'
        },
        {
            title: '名称',
            dataIndex: 'name2',
            key: 'name2',
            align: 'center'
        },
        {
            title: '英文名称',
            dataIndex: 'en_name',
            key: 'en_name',
            align: 'center'
        }];
        return(
            <div className="main">
                <ETable
                    tip="查询表格"
                    size={"middle"}
                    columns={columns}
                    params={this.params}
                    listService={null} 
                />
            </div>
        )
    }
}

export default DetailContainer(DetailDemo)