import React,{ PureComponent }from 'react';
import { Input,Form } from 'antd';

class NewSupplier extends PureComponent{
    state = {
        suppliers: [],
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const { info } = this.props;
        const formItemLayout = {
            labelCol: {
              sm: { span: 6 },
            },
            wrapperCol: {
              sm: { span: 14 },
            },
          };
          return(
            <Form {...formItemLayout}>
                <Form.Item label="姓名">
                    {getFieldDecorator('supplier_name', {
                        initialValue: info.carrierName || '',
                        rules:[
                            {
                                required: true, message: "请填写商品说明"
                            }],
                    })(
                        <Input placeholder="请填写商品说明" />
                    )}
                </Form.Item>
                <Form.Item label="手机号">
                    {getFieldDecorator('mobile', {
                        initialValue: info.mobile || '',
                        rules:[
                            {
                                required: true, message: "请填写手机号"
                            }],
                    })(
                        <Input placeholder="请填写手机号" />
                    )}
                </Form.Item>
                <Form.Item label="法人姓名">
                    {getFieldDecorator('legal_person', {
                        initialValue: info.mobile || '',
                        rules:[
                            {
                                required: true, message: "请填写法人姓名"
                            }],
                    })(
                        <Input placeholder="请填写法人姓名" />
                    )}
                </Form.Item>
            </Form>
        )
    }
  
}

export default Form.create()(NewSupplier);

