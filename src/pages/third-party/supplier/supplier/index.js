import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import { suppliersService,supplierAddService } from 'service'
import { Badge,Modal,Button,Icon } from 'antd';
import NewSupplier from './new';


class Supplier extends React.PureComponent{

    state={
        visible: false,
        info: {}
    }

    params={}

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    reload=()=>{
        EDragTable.refresh(this.params);
    }

    showModal=()=>{
        this.setState({
            visible: true
        })
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return suppliersService(finalParams)
    }


    handleCancel=()=>{
        this.setState({
            visible: false
        })
    }

    handleOk=()=>{
        this.supplierForm.props.form.validateFieldsAndScroll((err,values)=>{
            if(!err){
                supplierAddService(values).then(res=>{
                    if(res){
                        this.setState({
                            visible: false,
                            info: {}
                        },()=>{
                            this.reload()
                        })
                    }
                })
            }
        })
    }


    render(){
        const columns = [
        {
            title: '姓名',
            dataIndex: 'supplier_name',
            key: 'supplier_name',
            align: 'center'
        },
        {
            title: '联系方式',
            dataIndex: 'mobile',
            key: 'mobile',
            align: 'center'
        },
        {
            title: '法人代表',
            dataIndex: 'legal_person',
            key: 'legal_person',
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "合作中",
                    1: "已取消合作",
                    2: "已删除"
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        },
        {
            title: '更新人',
            dataIndex: 'update_user',
            key: 'update_user',
            align: 'center'
        },
        {
            title: '更新时间',
            dataIndex: 'update_time',
            key: 'update_time',
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operater',
            key: 'operater',
            align: 'center',
            render: t=>{
                return(
                    <>
                        <Button type="link" onClick={this.handleEdit}>编辑</Button>
                        <Button type="link" onClick={this.handleDetail}>详情</Button>
                    </>
                )
            }
        }];
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入供货商名称/联系方式"
                            },
                            {
                                field: "status",
                                label: "状态",
                                type: "select",
                                placeholder: "请选择供应商状态",
                                initialValue: '-1',
                                options:[
                                    { id: "-1", name:"全部" },
                                    { id: "0", name:"合作中" },
                                    { id: "1", name:"合作中止" },
                                    { id: "2", name:"已删除" }
                                ]
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <div className="operate-area">
                        <Button type="primary" onClick={this.showModal} >新增</Button>
                        <Icon type="reload" onClick={this.reload} className="reload-btn" />
                    </div>
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="供应商列表"
                      size={"middle"}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                    <Modal
                        title="添加承运人"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        width={720}
                        destroyOnClose={true}
                        >
                        <NewSupplier info={this.state.info} wrappedComponentRef={(form) => this.supplierForm = form}/>
                    </Modal>
                </div>
            </>
        )
    }
}

export default Supplier