import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import { carriersService,carrierAddService } from 'service'
import NewCarrierForm from './new';
import { Badge,Button,Icon,Modal} from 'antd';


class Carrier extends React.PureComponent{

    state={
        visible: false,
        info: {}
    }

    params={}

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    reload=()=>{
        EDragTable.refresh(this.params);
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return carriersService(finalParams)
    }

    showModal=()=>{
        this.setState({
            visible: true
        })
    }

    handleCancel=()=>{
        this.setState({
            visible: false
        })
    }

    handleOk=()=>{
        this.carrierForm.props.form.validateFieldsAndScroll((err,values)=>{
            if(!err){
                carrierAddService(values).then(res=>{
                    if(res){
                        this.setState({
                            visible: false,
                            info: {}
                        },()=>{
                            this.reload()
                        })
                    }
                })
            }
        })
    }
    render(){
        const columns = [
        {
            title: '姓名',
            dataIndex: 'carrier_name',
            key: 'carrier_name',
            align: 'center'
        },
        {
            title: '联系方式',
            dataIndex: 'mobile',
            key: 'mobile',
            align: 'center'
        },
        {
            title: '所属供应商',
            dataIndex: 'supplier.supplier_name',
            key: 'supplier.supplier_name',
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "正常",
                    1: "禁用",
                    2: "已删除"
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        },
        {
            title: '创建时间',
            dataIndex: 'create_time',
            key: 'create_time',
            align: 'center'
        },
        {
            title: '更新时间',
            dataIndex: 'update_time',
            key: 'update_time',
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operater',
            key: 'operater',
            align: 'center',
            render: t=>{
                return(
                    <>
                        <Button type="link" onClick={this.handleEdit}>编辑</Button>
                        <Button type="link" onClick={this.handleDetail}>详情</Button>
                    </>
                )
            }
        }];
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入商标名称/商标码查询"
                            },
                            {
                                field: "status",
                                label: "状态",
                                type: "select",
                                placeholder: "请选择商标状态",
                                initialValue: '-1',
                                options:[
                                    { id: "-1", name:"全部" },
                                    { id: "0", name:"正常" },
                                    { id: "1", name:"禁用" },
                                    { id: "2", name:"已删除" }
                                ]
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <div className="operate-area">
                        <Button type="primary" onClick={this.showModal} >新增</Button>
                        <Icon type="reload" onClick={this.reload} className="reload-btn" />
                    </div>
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="承运人列表"
                      size={"middle"}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                </div>
                <Modal
                    title="添加承运人"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    width={720}
                    destroyOnClose={true}
                    >
                    <NewCarrierForm info={this.state.info} wrappedComponentRef={(form) => this.carrierForm = form}/>
                </Modal>
            </>
        )
    }
}

export default Carrier