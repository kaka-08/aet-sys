import React,{ PureComponent }from 'react';
import { Select,Input,Form } from 'antd';
import { supplierAllService } from 'service';

const Option = Select.Option;
class NewCarrier extends PureComponent{
    state = {
        suppliers: [],
    }

    componentDidMount(){
        supplierAllService().then(res=>{
            this.setState({
                suppliers: res
            })
        }) 
    }

    render(){
        const { suppliers } = this.state;
        const { getFieldDecorator } = this.props.form;
        const { info } = this.props;
        const formItemLayout = {
            labelCol: {
              sm: { span: 6 },
            },
            wrapperCol: {
              sm: { span: 14 },
            },
          };
          return(
            <Form {...formItemLayout}>
                <Form.Item label="所属供应商">
                    {getFieldDecorator('supplier_id', {
                        initialValue: info.remark,
                        rules:[
                            {
                                required: true, message: "请填写商品说明"
                            }],
                    })(
                        <Select placeholder="请填写商品说明">
                            {
                                suppliers.map(item=><Option value={item.id} key={item.id}>{item.name}</Option>)
                            }
                        </Select>
                    )}
                </Form.Item>
                <Form.Item label="姓名">
                    {getFieldDecorator('carrier_name', {
                        initialValue: info.carrierName || '',
                        rules:[
                            {
                                required: true, message: "请填写商品说明"
                            }],
                    })(
                        <Input placeholder="请填写商品说明" />
                    )}
                </Form.Item>
                <Form.Item label="手机号">
                    {getFieldDecorator('mobile', {
                        initialValue: info.mobile || '',
                        rules:[
                            {
                                required: true, message: "请填写商品说明"
                            }],
                    })(
                        <Input placeholder="请填写商品说明" />
                    )}
                </Form.Item>
            </Form>
        )
    }
  
}

export default Form.create()(NewCarrier);

