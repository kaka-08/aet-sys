import React, { useState } from 'react';
import { Card,Col, Row,Badge,Button } from 'antd';
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import { wmListService } from 'service'
import './list.less';

export default function CurrentStock(){

    const params = {};

    const [count,useCount] = useState(0);

    const doRequestList=(_params={})=>{
        let finalParams = Object.assign(params,{..._params})
        return wmListService(finalParams)
    }
    const columns = [
        {
            title: '商品名称',
            dataIndex: 'product_name',
            key: 'product_name',
            align: 'center'
        },
        {
            title: '模版',
            dataIndex: 'sku_tpl_name',
            key: 'sku_tpl_name',
            align: 'center'
        },
        {
            title: '类别',
            dataIndex: 'category_name',
            key: 'category_name',
            align: 'center'
        },
        {
            title: '库存（箱）',
            dataIndex: 'stock',
            key: 'stock',
            align: 'center'
        },
        {
            title: '预计售价',
            dataIndex: 'price',
            key: 'price',
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            align: 'center',
            render: t=>{
                let kvMaps = {
                    0: "正常",
                    1: "废弃",
                    2: "已删除"
                }[t];
                const colors = {
                    0: '#87d068',
                    1: 'red',
                    2: 'gray'
                }
                return <Badge color={colors[t]} text={kvMaps} />
            }
        }];
    return(
        <>
            <div>
                <EFilterForm handleFilter={null} formList={
                    [
                        {
                            field: "search",
                            label: "关键词",
                            type: "input",
                            placeholder: "请输入商品名称/分类查看",
                            colSpan: 8
                        }
                    ]
                }/>
            </div>
            <div className="main">
                <EDragTable
                    updateSelectKey={function(){}}
                    tip="商品列表"
                    size={"middle"}
                    rowSelection={null}
                    columns={columns}
                    listService={doRequestList} 
                />
            </div>
        </>
    )
}
