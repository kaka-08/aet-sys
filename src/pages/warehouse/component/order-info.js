import React from 'react';
import { Popover,Card,Table } from 'antd';
import './order-info.less';
import { expirationUnit } from 'constants/constants';

export default class OrderInfo extends React.PureComponent{

    state = {
        carrierVisible: false,
        supplieVisible: false
    }

    handleCarrierVisibleChange = visible => {
        this.setState({ carrierVisible: visible });
    };

    handleSupplierVisibleChange = visible => {
        this.setState({ supplieVisible: visible });
    };

    render(){
        const columns = [
            {
                title: '商品',
                dataIndex: 'name',
                key: 'name',
                render: (t,r)=><span>{r.sku_name}</span>
            },
            {
                title: '生产日期',
                dataIndex: 'production_date',
                key: 'production_date',
            },
            {
                title: '保质期',
                dataIndex: 'sku_tpl.expiration_date',
                key: 'sku_tpl.expiration_date',
                render: (t,r)=>t ? t+ expirationUnit.get(r.sku_tpl.expiration_unit) : '--'
            },
            {
                title: '单价（¥）',
                dataIndex: 'single_price',
                key: 'single_price',
                render: (t,r)=><span>{ ( r.purchasing_price / r.stock ).toFixed(2)  }</span>
            },
            {
                title: '数量',
                dataIndex: 'stock',
                key: 'stock',
            },
            {
                title: '总价（¥）',
                dataIndex: 'purchasing_price',
                key: 'purchasing_price',
                render: (t,r)=>t.toFixed(2)
            },
          ];
        
        const { info,skus,totalFee } = this.props;
        const { carrierVisible,supplieVisible } = this.state;
        return(
            <>
                <Card title="基本信息" bordered={false}>
                    <div className="info-box">
                        <span><strong>【订单号】 </strong> { info.order_no }</span>
                        <span>
                            <strong>【供货商】 </strong> 
                                <Popover
                                    content={<div>
                                        <p>【姓名】 {info.supplier_name}</p>
                                        {
                                            info.supplier_mobile && <p>【联系方式】 <a>{info.supplier_mobile}</a></p>
                                        }
                                    </div>}
                                    title="供货商"
                                    placement="topLeft"
                                    visible={supplieVisible}
                                    onVisibleChange={this.handleSupplierVisibleChange}
                                >
                                    <a>{ info.supplier_name }</a>
                                </Popover>
                        </span>
                        <span>
                            <strong>【承运人】 </strong>  
                            <Popover
                                content={<div>
                                    <p>【姓名】 {info.carrier_name}</p>
                                    {
                                        info.carrier_mobile && <p>【联系方式】 <a>{info.carrier_mobile}</a></p>
                                    }
                                </div>}
                                title="承运人"
                                placement="topLeft"
                                visible={carrierVisible}
                                onVisibleChange={this.handleCarrierVisibleChange}
                            >
                                <a>{ info.carrier_name }</a>
                            </Popover>
                        </span>
                        <p><strong>【备注】 </strong>  { info.remark } </p>
                    </div>
                </Card>
                <Card title="入库商品" bordered={false}>
                    <Table 
                        pagination={false} 
                        bordered={false} 
                        rowKey={record=>record.id} 
                        size={"small"} 
                        className="seft-table"
                        columns={columns} 
                        dataSource={skus}
                        footer={()=><span>订单总金额：<em>{totalFee.toFixed(2)}</em> 元</span>} 
                    />
                </Card>
            </>
        )
    }
   
}