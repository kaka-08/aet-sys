import React from 'react'
import EDragTable from 'components/EDragTable';
import EFilterForm from 'components/EFilterForm';
import OrderInfo from './component/order-info';
import { 
    warehouseInsService,warehouseInDetailService } from 'service'
import Utils from 'utils/Util';
import { Button,Icon,Tooltip,Modal,Card,Table } from 'antd';
import './in.less'


class In extends React.PureComponent{

    state={
        visible: false,
        totalFee: 0,
        orderInfo: {
            sku_list: []
        }
    }

    params={}

    handleFilter=(params)=>{
        EDragTable.refresh(params);
    }

    doRequestList=(params={})=>{
        let finalParams = Object.assign(this.params,{...params})
        return warehouseInsService(finalParams)
    }


    gotoAdd=()=>{
        Utils.goBlankPage('/#/warehouse/new-in')
    }

    handleDetail=(e,id)=>{
        e.stopPropagation();
        warehouseInDetailService(id).then(res=>{
            let fee = 0;
            res.sku_list.forEach(item=>fee+=item.purchasing_price)
            this.setState({
                orderInfo: res,
                visible: true,
                totalFee: fee
            })
        })
    }

    handleCancel=()=>{
        this.setState({
            visible: false
        })
    }

    render(){
        const { visible,orderInfo,totalFee } = this.state;  
        const columns = [
            {
                title: '订单号',
                dataIndex: 'order_no',
                key: 'order_no',
                align: 'center',
                render: (t,r)=><Tooltip title="点击查看订单详情">
                    <a onClick={(e)=>this.handleDetail(e,r.id)}>{t}</a>
                </Tooltip>
            },
            {
                title: '供货商',
                dataIndex: 'supplier_name',
                key: 'supplier_name',
                align: 'center'
            },
            {
                title: '送货人',
                dataIndex: 'carrier_name',
                key: 'carrier_name',
                align: 'center'
            },
            {
                title: '收货人',
                dataIndex: 'consignee',
                key: 'consignee',
                align: 'center',
                render: t => t || '--'
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
                align: 'center',
                render: t => t || '--'
            },
            {
                title: '入库时间',
                dataIndex: 'create_time',
                key: 'update_create_timetime',
                align: 'center'
            }];
        return(
            <>
                <div>
                    <EFilterForm handleFilter={this.handleFilter} formList={
                        [
                            {
                                field: "search",
                                label: "关键词",
                                type: "input",
                                placeholder: "请输入供应商/送货人/订单号"
                            }
                        ]
                    }/>
                </div>
                <div className="main">
                    <div className="operate-area">
                        <Button type="primary" onClick={this.gotoAdd} >新增</Button>
                        <Icon type="reload" onClick={this.reload} className="reload-btn" />
                    </div>
                    <EDragTable
                      updateSelectKey={function(){}}
                      tip="入库记录"
                      size={"middle"}
                      columns={columns}
                      listService={this.doRequestList} 
                    />
                </div>
                <Modal
                    title="详情"
                    visible={visible}
                    footer={null}
                    width={1000}
                    onCancel={this.handleCancel}
                    >
                       <OrderInfo info={orderInfo} skus={orderInfo.sku_list} totalFee={totalFee}/>
                </Modal>
            </>
        )
    }
}

export default In