import React from 'react';
import { Button, message } from 'antd';
import OrderInfo from '../component/order-info';
import { connect } from 'react-redux';
import Utils from 'utils/Util';
import { skuBatchAddService,doWarehouseInService } from 'service';
import './index.less';


class Step3 extends React.PureComponent{

    doPrev=()=>{
        const { prev } = this.props;
        prev(2);
    }

    doCreate=()=>{
        const { clearData } = this.props;
        const { orderInfo, skus } = this.props;
        let params = JSON.parse(JSON.stringify(orderInfo));
        skuBatchAddService(skus).then(res=>{
            if(res && res.length){
                params.sku_ids = res.join();
                doWarehouseInService(params).then(res=>{
                    if(res){
                        message.success("入库成功");
                        setTimeout(()=>{
                            clearData();
                            Utils.goBlankPage('/warehouse/in')
                        },1000)
                    }
                })
            }
        })
    }
    
    render(){
        const { orderInfo, skus } = this.props;
        return(
            <div className="main step3">
                <OrderInfo info={orderInfo} skus={skus} totalFee={0} />
                <div style={{textAlign: 'right',paddingTop: 20}}>
                    <Button onClick={this.doCancel} style={{marginRight: 20}}>取消入库</Button>
                    <Button onClick={this.doPrev} style={{marginRight: 20}}>返回订单信息</Button>
                    <Button onClick={this.doDraft} style={{marginRight: 20, background: 'green',color:"#ffffff"}}>暂存草稿</Button>
                    <Button onClick={this.doCreate} type="primary">入库</Button>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    const { warehouseIn } = state;
    return { 
        orderInfo: warehouseIn.orderInfo,
        skus: warehouseIn.skus
    }
}

const mapDispatchToProps = dispatch => {
    return {
        clearData: () => dispatch({ type:"warehouse_in/clearData"})
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Step3);