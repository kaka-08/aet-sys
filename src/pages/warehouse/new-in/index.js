import React from 'react';
import { Steps } from 'antd';
import './index.less';
import Step1 from './step1';
import Step2 from './step2';
import Step3 from './step3';

const { Step } = Steps;
class NewIn extends React.PureComponent{

    state={
        current: 0,
        from: 0
    }

    next = (from)=>{
        const current = this.state.current + 1;
        this.setState({ current,from });
    }

    prev =(from)=> {
        const current = this.state.current - 1;
        this.setState({ current,from });
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        console.log('nextProps %o',nextProps);
    }

    render(){
        const { current,from } = this.state;
        const steps = [
            {
              title: '入库商品',
              content: <Step1 from={from} current={current} next={this.next} prev={this.prev}/>,
            },
            {
              title: '订单信息',
              content: <Step2 current={current} next={this.next} prev={this.prev}/>,
            },
            {
              title: '预览',
              content:  <Step3 current={current} prev={this.prev}/>,
            },
          ];
        return (
            <>
                <div className="main">
                    <p>为了保障数据的准确性，我们特此推出了以下入库流程，库管需要精确录入订单以保障数据的准确性</p>
                    <Steps progressDot current={3} direction="vertical">
                        <Step title="第一步" description="填写入库的商品" />
                        <Step title="第二步" description="录入送货人信息" />
                        <Step title="第三步" description="核对数据单，无误则可入库" />
                    </Steps>
                </div>
                <div className="main">
                    <Steps current={current}>
                        {steps.map(item => (
                            <Step key={item.title} title={item.title} />
                        ))}
                    </Steps>
                    <div className="steps-content">{steps[current].content}</div>
                </div>
            </>
        );
    }
}

export default NewIn