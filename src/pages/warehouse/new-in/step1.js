import React from 'react';
import { Card,Form,Row,Col,Button,Select,DatePicker,InputNumber,Alert,Table,message } from 'antd';
import { goodsSkuTplsService } from 'service';
import moment from 'moment';
import { connect } from 'react-redux';

const fmt = 'YYYY-MM-DD HH:mm:ss';
const Option = Select.Option;
class Step1 extends React.PureComponent{

    state={
        goodsSkuTpls: [],
        skus:[],
        tempSkus:[],
        tempSkuParams: {},
        isFinished: false,
        isAdd: false,
    }

    componentDidMount(){
        const { skus } = this.props;
        goodsSkuTplsService().then(res=>{
            this.setState({
                goodsSkuTpls:res
            })
        })
        this.setState({
            tempSkus: [].concat(skus)
        })
    }

    showAddSku=()=>{
        this.setState({
            isAdd: true
        })
    }

    AddTempSku=()=>{
        let { tempSkuParams,tempSkus } = this.state;
        this.props.form.validateFields((err,values)=>{
            if(!err){
                values.production_date = moment(values.production_date).format(fmt);
                tempSkuParams.sku_name = tempSkuParams.name + '-' + tempSkuParams.sku_tpl_name;
                let _tempRecord = tempSkus.find(item=>item.id === tempSkuParams.id);
                if(_tempRecord){
                    //如果有,修改原数据
                    _tempRecord = {
                        ..._tempRecord,
                        ...values
                    }
                }else{
                    Object.assign(tempSkuParams,values);
                    tempSkus.push(tempSkuParams);
                }
                this.setState({
                    tempSkus,
                    tempSkuParams: {},
                    isAdd: false
                })
            }
        })
    }

    //完成,批量生成sku,且进入下一步
    patchAddSku=()=>{
        this.setState({
            isAdd: false,
            isFinished: true
        })
    }


    handleSelect=(v)=>{
        let { goodsSkuTpls } = this.state;
        let record = goodsSkuTpls.find(item=>item.id === v);
        this.setState({
            tempSkuParams: record
        })
    }

    editSku=(e,record)=>{
        e.stopPropagation();
        this.setState({
            tempSkuParams: record,
            isAdd: true
        },()=>{
            this.props.form.setFieldsValue({
                product_id: record.id,
                purchasing_price: record.purchasing_price,
                stock: record.stock,
                production_date: moment(record.production_date, fmt),
            });
        })
    }

    removeSku=(e,index)=>{
        e.stopPropagation();
        let { tempSkus } = this.state;
        tempSkus.splice(index,1);
        this.setState({tempSkus})
    }

    cancelEdit=(e)=>{
        e.stopPropagation();
        this.setState({
            isAdd: false
        })
    }

    doNext=()=>{
        const { updateSkus,next } = this.props;
        let { tempSkus } = this.state;
        updateSkus(tempSkus);
        next(2)
    }

    render(){
        const formItemLayout = {
            labelCol: {
              sm: { span: 7 }
            },
          };
          const formItemLayout1 = {
            labelCol: {
              sm: { span: 4 }
            },
          };
        const { getFieldDecorator } = this.props.form;
        const { from } = this.props;
        const { goodsSkuTpls,isFinished,tempSkus,isAdd } = this.state;
        const columns = [
            {
                title: '商品',
                dataIndex: 'name',
                key: 'name',
                render: (t,r)=><span>{t}【规模】{r.sku_tpl_name}</span>
            },
            {
                title: '总价（¥）',
                dataIndex: 'purchasing_price',
                key: 'purchasing_price',
            },
            {
                title: '数量',
                dataIndex: 'stock',
                key: 'stock',
            },
            {
                title: '生产日期',
                dataIndex: 'production_date',
                key: 'production_date',
            },
            {
              title: '操作',
              key: 'action',
              render: (text, record,index) => (
                  <span>
                     <a onClick={e=>this.editSku(e,record)} style={{ marginRight: 16 }}> 修改</a>
                     <a onClick={e=>this.removeSku(e,index)}> 删除</a>
                  </span>
              ),
            },
        ];
        return(
            <>
                <Alert 
                    className="goods-info-alert" 
                    message="入库说明"
                    description="系统会以sku的维度统计商品，不同日期录入的商品sku也不同，后期随着系统的更新，我们会引入您创建过的sku模版，方便您快速入库"
                    type="info" showIcon />
                <div className="goods-card">
                    {
                        tempSkus.length > 0
                        &&
                        <Table pagination={false} rowKey={record=>record.id} size={"small"} columns={columns} dataSource={tempSkus} />
                    }
                    {
                        isFinished
                        ?
                        null
                        :
                        <Form>
                            {
                                isAdd
                                ?
                                <Card className="goods-card-item" bordered={false}>
                                    <Row>
                                        <Col span={9}>
                                            <Form.Item {...formItemLayout1} label="商品">
                                                {
                                                    getFieldDecorator('product_id', {
                                                        rules: [
                                                        { required: true,message: '请选择商品'},
                                                        ],
                                                    })(<Select className="select-box" style={{width: 280}} onSelect={this.handleSelect}>
                                                        {
                                                            goodsSkuTpls.map(item=><Option key={item.id} value={item.id}>{<span className="goods-name">{item.name} 【规格】<span className="sku-tpl-name">{item.sku_tpl_name}</span></span>}</Option>)
                                                        }
                                                    </Select>)
                                                }
                                            </Form.Item>
                                        </Col>
                                        <Col span={4}>
                                            <Form.Item {...formItemLayout} label="数量">
                                                {
                                                    getFieldDecorator('stock', {
                                                        rules: [
                                                        { required: true,message: '请填写数量'},
                                                        ],
                                                    })(<InputNumber />)
                                                }
                                            </Form.Item>
                                        </Col>
                                        <Col span={5}>
                                            <Form.Item {...formItemLayout} label="总价（¥）">
                                                {
                                                    getFieldDecorator('purchasing_price', {
                                                        rules: [
                                                        { required: true,message: '请填写总价'},
                                                        ],
                                                    })(<InputNumber style={{width: 100}}/>)
                                                }
                                            </Form.Item>
                                        </Col>
                                        <Col span={6}>
                                            <Form.Item {...formItemLayout} label="生产日期">
                                                {
                                                    getFieldDecorator('production_date', {
                                                        rules: [
                                                        { required: true,message: '请选择生产日期'},
                                                        ],
                                                    })(<DatePicker style={{width: 80}} showTime onOk={this.onOk} />)
                                                }
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row  style={{textAlign: 'right',marginTop: 20}}>
                                        <Button onClick={this.cancelEdit} style={{marginRight: 15}}>取消</Button>
                                        <Button onClick={this.AddTempSku} type="primary">入库</Button>
                                    </Row>
                                </Card>
                                :
                                null
                            }
                            {
                                isAdd 
                                ?
                                null
                                :
                                from === 0 &&
                                <Form.Item style={{textAlign: 'center',marginTop: 20}}>
                                    <Button icon={"plus"} type="primary" onClick={this.showAddSku}>
                                        { tempSkus.length ? "继续添加" : "添加入库商品" }
                                    </Button>
                                    {
                                        tempSkus.length > 0
                                        &&
                                        <Button onClick={this.patchAddSku} style={{marginLeft: 20}}>
                                            完成
                                        </Button>
                                    }
                                </Form.Item>
                            }
                        </Form>
                    }
                </div>
                <div style={{textAlign: 'right',paddingTop: 20}}>
                    <Button onClick={this.doNext} disabled={from === 0 ? ( isFinished ? false : true ) : false } type="primary">录入订单信息</Button>
                </div>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { warehouseIn } = state;
    return { 
        skus: warehouseIn.skus,
    }
  }

const mapDispatchToProps = dispatch => {
    return {
        updateSkus: (skus) => dispatch({ type:"warehouse_in/updateSkus",payload: skus})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(Step1));
