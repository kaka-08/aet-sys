import React from 'react';
import { Form,Input,Select,Button } from 'antd';
import {
    supplierAllService,
    carriersAllService,
} from 'service';
import { connect } from 'react-redux';


const Option = Select.Option;
const { TextArea } = Input;
class Step2 extends React.PureComponent{

    state={
        suppliers:[],
        carriers:[],
        isFinished: false
    }

    componentDidMount(){
        const { orderInfo } = this.props;
        supplierAllService().then(res=>{
            this.setState({
                suppliers:res
            })
        })
        this.props.form.setFieldsValue(orderInfo)
    }

    doPrev=()=>{
        const { prev } = this.props;
        prev(1);
    }

    doNext=()=>{
        const { next } = this.props;
        this.props.form.validateFields((err,values)=>{
            if(!err){
                this.props.updateOrderInfo(values);
                next(3);
            }
        })
    }


    saveName=(v,option,key)=>{
        const { updateCarriers } = this.props;
        const { setFieldsValue } = this.props.form;
        let name = option.props.children;
        setFieldsValue({[key]:name});
        if(key === 'supplier_name'){
            setFieldsValue({supplier_mobile: option.key});
            carriersAllService(v).then(res=>{
                updateCarriers(res)
            })
        }
        if( key === 'carrier_name'){
            setFieldsValue({carrier_mobile: option.key});
        }
    }

    render(){
        const formItemLayout = {
            labelCol: {
                sm: { span: 8 }
            },
            wrapperCol: {
                sm: { span: 16 }
            }
          };
        const { carriers } = this.props;
        const { getFieldDecorator } = this.props.form;
        const { suppliers } = this.state;
        return(
            <div className="main">
                <Form>
                    <Form.Item {...formItemLayout} label="入库单号">
                        {
                        getFieldDecorator('order_no', {
                            rules: [
                            { required: true,message: '请录入单号'},
                            ],
                        })(<Input style={{width: 250}}/>) 
                        }
                        <span style={{marginLeft: 15}}>填写数据单上的订单编号</span>
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="所属供货商">
                        {getFieldDecorator('supplier_id', {
                            rules: [
                            { required: true,message: '请选择模板'},
                            ],
                        })(<Select style={{width: 350}} className="select-box" onSelect={(v,option)=>this.saveName(v,option,'supplier_name')}>
                            {
                                suppliers.map(item=><Option key={item.mobile} value={item.id}>{item.name}</Option>)
                            }
                        </Select>)}
                    </Form.Item>
                    <Form.Item label="供货商名称" className="form-item-hide">{getFieldDecorator('supplier_name', {})(<span />)}</Form.Item>
                    <Form.Item label="供货商手机号" className="form-item-hide">{getFieldDecorator('supplier_mobile', {})(<span />)}</Form.Item>
                    <Form.Item {...formItemLayout} label="送货人">
                        {getFieldDecorator('carrier_id', {
                            rules: [
                            { required: false,message: '请选择模板'},
                            ],
                        })(<Select style={{width: 250}} className="select-box" onSelect={(v,option)=>this.saveName(v,option,'carrier_name')}>
                            {
                                carriers.map(item=><Option key={item.mobile} value={item.id}>{item.name}</Option>)
                            }
                        </Select>)}
                    </Form.Item>
                    <Form.Item label="送货人名称" className="form-item-hide">{getFieldDecorator('carrier_name', {})(<span />)}</Form.Item>
                    <Form.Item label="送货人手机号" className="form-item-hide">{getFieldDecorator('carrier_mobile', {})(<span />)}</Form.Item>
                    <Form.Item {...formItemLayout} label="备注">
                        {getFieldDecorator('remark', {
                            rules:[
                                {
                                    required: false, message: "请填写商品说明"
                                }],
                        })(
                            <TextArea
                                style={{width: 500}}
                                placeholder="备注信息"
                                autoSize={{ minRows: 3, maxRows: 5 }}
                            />
                        )}
                    </Form.Item>
                </Form>
                <div style={{textAlign: 'right',paddingTop: 20}}>
                    <Button onClick={this.doPrev} style={{marginRight: 20}}>返回入库列表</Button>
                    <Button onClick={this.doNext} type="primary">确认并预览</Button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { warehouseIn,carrier } = state;
    return { 
        orderInfo: warehouseIn.orderInfo,
        carriers: carrier.list
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateOrderInfo: (orderInfo) => dispatch({ type:"warehouse_in/updateOrderInfo",payload: orderInfo}),
        updateCarriers: (carriers) => dispatch({ type:"carrier/updateCarriers",payload: carriers}),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(Step2));


 