import React from 'react'
import ETable from 'components/ETable';
import { queryEnvService } from 'service';
import { Table } from 'antd';


export default class Home extends React.PureComponent{

    state = {
        dashboards: []
    }

    handleClick=()=>{
        ETable.refresh();
    }

    componentDidMount(){
        const data = [{"changed_by_name":"liqinglong \u674e\u9752\u9f99","changed_on":"2020-04-21T17:26:43","classification":"\u7efc\u5408\u6570\u636e","creator":"<a href=\"/superset/profile/liqinglong/\">\u674e\u9752\u9f99</a>","dashboard_link":"<a href=\"/superset/dashboard/98/?preselect_filters=%7B%22321%22%3A%20%7B%22__time_range%22%3A%20%22Last%201%20years%22%2C%20%22province%22%3A%20%5B%22%5Cu5168%5Cu56fd%22%5D%7D%7D\">\u8f66\u8f86\u4f7f\u7528\u5173\u952e\u6307\u6807\u2014\u7701</a>","dashboard_title":"\u8f66\u8f86\u4f7f\u7528\u5173\u952e\u6307\u6807\u2014\u7701","display_order":17,"id":98,"main_metrics":"\u8ba2\u5355\u6570/\u8f66\u6548/\u964d\u6c34\u91cf/\u6e29\u5ea6/\u603b\u6536\u5165/\u626b\u7801\u4e0d\u53ef\u7528\u7387/\u632a\u8f66\u91cf/\u6295\u653e\u8f66\u8f86\u6570","modified":"<span class=\"no-wrap\">a day ago</span>","update_frequency":"\u5929","url":"/superset/dashboard/98/?preselect_filters=%7B%22321%22%3A%20%7B%22__time_range%22%3A%20%22Last%201%20years%22%2C%20%22province%22%3A%20%5B%22%5Cu5168%5Cu56fd%22%5D%7D%7D"},{"changed_by_name":"zhangshuai \u5f20\u5e05","changed_on":"2020-03-27T13:34:04","classification":"\u7528\u6237\u5206\u6790","creator":"<a href=\"/superset/profile/zhangshuai/\">\u5f20\u5e05</a>","dashboard_link":"<a href=\"/superset/dashboard/79/?preselect_filters=%7B%22306%22%3A%20%7B%22__time_range%22%3A%20%22Last%206%20months%22%2C%20%22op_mode%22%3A%20%5B%22%5Cu5168%5Cu90e8%22%5D%2C%20%22province%22%3A%20%5B%22%5Cu5168%5Cu56fd%22%5D%7D%7D\">\u6536\u5165\u6307\u6807\u7edf\u8ba1</a>","dashboard_title":"\u6536\u5165\u6307\u6807\u7edf\u8ba1","display_order":825,"id":79,"main_metrics":"\u7528\u6237\u6570/\u8ba2\u5355\u6570/\u5361\u6536\u5165/\u603b\u6536\u5165","modified":"<span class=\"no-wrap\">26 days ago</span>","update_frequency":"\u5929","url":"/superset/dashboard/79/?preselect_filters=%7B%22306%22%3A%20%7B%22__time_range%22%3A%20%22Last%206%20months%22%2C%20%22op_mode%22%3A%20%5B%22%5Cu5168%5Cu90e8%22%5D%2C%20%22province%22%3A%20%5B%22%5Cu5168%5Cu56fd%22%5D%7D%7D"},{"changed_by_name":"liqinglong \u674e\u9752\u9f99","changed_on":"2020-03-26T11:13:12","classification":"\u5730\u7406\u4fe1\u606f","creator":"<a href=\"/superset/profile/liqinglong/\">\u674e\u9752\u9f99</a>","dashboard_link":"<a href=\"/superset/dashboard/90/\">\u677e\u679c\u5f00\u57ce\u7edf\u8ba1\u5730\u56fe</a>","dashboard_title":"\u677e\u679c\u5f00\u57ce\u7edf\u8ba1\u5730\u56fe","display_order":3203,"id":90,"main_metrics":"\u7535\u5355\u8f66\u5df2\u5f00\u57ce\u6570/\u76f4\u8425\u6570/\u4ee3\u7406\u57ce\u6570/\u57ce\u5e02\u6628\u65e5\u8ba2\u5355/\u5386\u53f2\u603b\u6536\u76ca","modified":"<span class=\"no-wrap\">27 days ago</span>","update_frequency":"\u5929","url":"/superset/dashboard/90/"},{"changed_by_name":"liqinglong \u674e\u9752\u9f99","changed_on":"2020-02-21T17:39:38","classification":"\u8ba2\u5355","creator":"<a href=\"/superset/profile/liqinglong/\">\u674e\u9752\u9f99</a>","dashboard_link":"<a href=\"/superset/dashboard/44/\">\u677e\u679c\u4ff1\u4e50\u90e8\u9f99\u864e\u699c</a>","dashboard_title":"\u677e\u679c\u4ff1\u4e50\u90e8\u9f99\u864e\u699c","display_order":435,"id":44,"main_metrics":"\u57ce\u5e02\u8ba2\u5355\u65b0\u9ad8/\u8f66\u6548\u65b0\u9ad8","modified":"<span class=\"no-wrap\">2 months ago</span>","update_frequency":"\u5929","url":"/superset/dashboard/44/"}]
        this.calAndGenerateDashboards("",data);
    }
    calAndGenerateDashboards =(search="", allDashboards = [])=>{
        let tempDashBoard = allDashboards.filter(item => item.dashboard_title.indexOf(search) > -1 );
        let dataSourceKeys = {};
        let tempDataSource = [];
        let len = tempDashBoard.length;
        if(len > 1){
            // O(n)
            for(let i = len-1;i>0;i--){
                let dashboardType = tempDashBoard[i].classification;  //看板类型
                if(!dataSourceKeys[dashboardType]){
                    tempDataSource.push({
                        id: dashboardType + '0001',
                        classification: dashboardType,
                        dashboard_title: "",
                        update_frequency: "",
                        main_metrics: "",
                        children: []
                    });
                    dataSourceKeys[dashboardType] = true;
                }
            }
            //再来一次 O(n)
            for(let i in tempDataSource){
                if(dataSourceKeys[tempDataSource[i].classification]){
                    dataSourceKeys[tempDataSource[i].classification] = i
                }
            }
            // 组合表格数据
            for(let i in tempDashBoard){
                let tempIndex = dataSourceKeys[tempDashBoard[i].classification];
                if(tempIndex){
                    tempDataSource[tempIndex].children.push(tempDashBoard[i])
                }
            }

            // 对children进行排序
            for(let i in tempDataSource){
                tempDataSource[i].children.sort(function (a,b){ return a.display_order - b.display_order })
            }

            tempDataSource.sort(function (a,b){ return a.display_order - b.display_order });

            this.setState({ dashboards: tempDataSource });
        }else if(len == 1){
            this.setState({ dashboards: tempDashBoard });
        }else{
            this.setState({ dashboards: [] });
        }
    }

    handleFilter=(params)=>{
        this.params = {...params};
        ETable.refresh(this.params);
    }


    doRequestList=()=>{
        return queryEnvService()
    }

    handleChange=(e)=>{
    }

    doExpand=(expanded, record)=>{
        console.log(expanded, record)
    }

    render(){
        const columns = [
            { title: '看板类别', dataIndex: 'classification', key: 'classification' },
            { title: '看板', dataIndex: 'dashboard_title', key: 'dashboard_title' },
            { title: '主要指标', dataIndex: 'main_metrics', key: 'main_metrics' },
            { title: '更新频率', dataIndex: 'update_frequency', key: 'update_frequency' }];
        return(
            <div className="main">
                <Table 
                    rowKey={record => record.id}
                    columns={columns}
                    dataSource={this.state.dashboards}
                    onExpand={this.doExpand} 
                    pagination={false} />
            </div>
        )
    }
}