import React from 'react';
import './index.less'

export default function Pending() {
    return (
      <div className="main pending">
          <h1>页面准备中......</h1>
          <p>
            <span style = {{"--i":1}}>尽</span>
            <span style = {{"--i":2}}>请</span>
            <span style = {{"--i":3}}>期</span>
            <span style = {{"--i":4}}>待</span>
            <span style = {{"--i":5}}>吧</span>
            <span style = {{"--i":6}}>😊</span>
            <span style = {{"--i":7}}>😊</span>
            <span style = {{"--i":8}}>😊</span>
          </p>
      </div>
    );
  }