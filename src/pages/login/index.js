import React from 'react';
import { Form, Icon, Input, Button } from 'antd';
import { loginService } from 'service/login-out';
import { message } from 'antd';
import Store from 'utils/Store';
import './index.less';
class Login extends React.PureComponent {

  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
        if (!err) {
            loginService({params:values}).then(res=>{
                message.success("登录成功");
                Store.save("username",res.zh_name);
                Store.save("token",res.usertoken);
                setTimeout(()=>{
                    if(window.history.length > 1){
                        window.history.back()
                    }else{
                        this.props.history.push('/');
                    }
                },1000)
            })
        }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
        <div className="login-box">
            <div className="devOps-info">
                <h1 className="login-tip">功能介绍</h1>
                <ul>
                    <li>帮助开发测试人员搭建和管理环境，以便在变更后部署变更以测试;</li>
                    <li>帮助运营支持人员升级系统，扩展重建恢复系统，在升级后能够持续地掌握系统整体和各个栈的状态，从各个层面监控系统，伸缩系统，恢复系统。</li>
                    <li>帮助开发测试人员搭建和管理环境，以便在变更后部署变更以测试;</li>
                    <li>更多信息;</li>
                </ul>
            </div>
            <Form className="login-form">
                <h1 className="login-tip">Hi！欢迎访问DevOps，请登录～</h1>
                <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: '请输入账号' }],
                    })(
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="用户名"
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: '请输入密码' }],
                    })(
                        <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="密码"
                        />
                    )}
                    </Form.Item>
                <Form.Item>
                    <Button type="primary" onClick={this.handleSubmit} className="login-form-button">
                        登录
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
  }
}

export default Form.create({})(Login);