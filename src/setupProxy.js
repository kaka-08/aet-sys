const proxy = require('http-proxy-middleware');
module.exports = function(app) {
  // app.use(
  //   '/api',
  //   proxy({
  //     target: 'http://dev-devops-api-in.songguo7.com',
  //     pathRewrite:{
  //       '^/api/':'/'
  //     },
  //     changeOrigin: true,
  //   })
  // );
  app.use(
    '/v1',
    proxy({
      target: 'http://127.0.0.1:8080',
      pathRewrite:{},
      changeOrigin: true,
    })
  );
};


