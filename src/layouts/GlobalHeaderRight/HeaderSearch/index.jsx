import React from 'react';
import { Icon,AutoComplete } from 'antd';
import classnames from 'classnames';
import './index.less';

export default class HeaderSearch extends React.PureComponent{

    constructor(props) {
        super(props);
        this.ref = React.createRef();
        this.state = {
            visible: false
        }
      }

   


    showSearch=()=>{
        this.setState({
            visible: !this.state.visible
        },()=>{
            this.ref.current.focus()
        })
    }

    onSelect=()=>{

    }

    onBlur=()=>{
        this.setState({
            visible: false,
            autoFocus: false
        })
    }

    render(){
        const dataSource = ['gmail.com', '163.com', 'qq.com'];
        const { visible } = this.state;
        return(
            <div className="header-search" {...this.props}>
                <Icon type="search" style={{fontSize:16}} onClick={this.showSearch} />
                <AutoComplete
                    className={classnames(
                        "search-input",
                        {
                            "show":visible
                        }
                    )}
                    ref={this.ref}
                    onBlur={this.onBlur}
                    dataSource={dataSource}
                    style={{ width: 200 }}
                    onSelect={this.onSelect}
                    onSearch={this.onSearch}
                    placeholder="站内搜索"
                />
            </div>
        )
    }
}