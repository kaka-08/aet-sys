import React from 'react';
import { Menu, Icon,Avatar } from 'antd';
import { NavLink } from 'react-router-dom'
import HeaderDropdown from '../components/HeaderDropdown';
import './index.less';


class UserDropDown extends React.PureComponent{

    state={
        visible: false
    }

    handleVisibleChange = flag => {
        this.setState({ visible: flag });
    };

    render(){
        const { username } = this.props;
        const menu = (
            <Menu>
                <Menu.Item key="0">
                    <a href="http://www.alipay.com/"><Icon type="user" /> 个人中心</a>
                </Menu.Item>
                <Menu.Item key="1">
                    <a href="http://www.taobao.com/"><Icon type="setting" /> 个人设置</a> 
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="3">
                    <a href="http://www.taobao.com/"><Icon type="logout" /> 退出登录</a> 
                </Menu.Item>
            </Menu>
        )
        return(
            <div className="dropdowm-box">
                {
                    username 
                    ?
                    <HeaderDropdown
                        placement="bottomRight"
                        overlay={menu}
                        overlayClassName={"ant-dropdown"}
                        trigger={['click']}
                        visible={this.state.visible}
                        onVisibleChange={this.handleVisibleChange}
                        {...this.props}
                        >
                        <NavLink className="ant-dropdown-link" to="#" onClick={e => e.preventDefault()}>
                            <Avatar src="https://www.diyimei.net/upload/2018/1537192515751749.jpg" />
                            <span>{ username } </span>
                            <Icon type="down" />
                        </NavLink>
                    </HeaderDropdown>
                    :
                    <NavLink className="ant-dropdown-link" to="#" onClick={e => e.preventDefault()}>
                        <span>去登录</span>
                    </NavLink>
                }
               
            </div>
        )
    }
}


export default UserDropDown