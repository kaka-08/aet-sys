import React from 'react';
import UserDropDown from './UserDropdown';
import HeaderSearch from './HeaderSearch';
import NoticeIconView from './NoticeIconView';
import ScanHistory from './ScanHistory';
import './index.less';

export default class GlobalHeaderRight extends React.Component{
    render(){
        return(
            <div className="global-header-right">
                {/* 访问历史 */}
                <ScanHistory style={{marginRight: 20}} {...this.props}/>
                {/* 搜索 */}
                <HeaderSearch style={{marginRight: 10}}/>
                {/* 通知栏 */}
                <NoticeIconView style={{marginRight: 10}} />
                {/* 用户 */}
                <UserDropDown username={"萨达阿斯顿"}/>
            </div>
        )
    }
}