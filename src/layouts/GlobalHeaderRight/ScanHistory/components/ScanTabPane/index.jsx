import React from 'react';
import { Row, Col } from 'antd';
import { NavLink } from 'react-router-dom'
import _ from 'lodash';
import './index.less';
import { connect } from 'react-redux';

class ScanTabPane extends React.PureComponent{

    static defaultProps = {
    };

    clearHistory=(e)=>{
        this.props.clearHistory()
    }

    render(){
        const lists = _.chunk(this.props.scanHistories,3);
        const len = lists.length;
        return(
            <div>
            {
                len
                ?
                <>
                {
                    lists.map((item,index)=>{
                        return (
                            <Row key={index}>
                                {
                                    item.map(sonItem=>{
                                        return(
                                            <Col span={8} className="scan-list-item" key={sonItem.menu_id}>
                                                <div className="scan-list-item-content">
                                                    <h1>{sonItem.name}</h1>
                                                    <Row>
                                                        {
                                                        sonItem.children.map(grandItem=>{
                                                            return(
                                                                <Col span={6} key={grandItem.menu_id}>
                                                                    <NavLink to={grandItem.url} onClick={e => e.preventDefault()}><span>{grandItem.name}</span></NavLink>
                                                                </Col> 
                                                            )
                                                        }) 
                                                        }
                                                    </Row>
                                                </div>
                                            </Col>
                                        )
                                    })
                                }
                            </Row>
                        )
                    })
                }
                <a className="history-tip" onClick={this.clearHistory}>点此清空</a>
                </>
                :
                <p className="empty-tip">暂无记录</p>
            }
            </div>
        )
    }    
}

const mapStateToProps = state => {
    const { scanHistory } = state;
    return { 
        scanHistories: scanHistory.list,
    }
  }

const mapDispatchToProps = dispatch => {
    return {
        clearHistory: () => dispatch({ type:"scan_history/clear", })
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(ScanTabPane)