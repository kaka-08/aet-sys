import React from 'react';
import { Icon } from 'antd'; 
import HeaderDropdown from '../components/HeaderDropdown';
import ScanTabPane from './components/ScanTabPane';
import './index.less';


export default class ScanHistory extends React.PureComponent{

    state={
        visible: false
    }

    handleVisibleChange = flag => {
        this.setState({ visible: flag });
    };

    render(){
        const { collapsed } = this.props;
        const scanList = (
            <>
                <ScanTabPane />
            </>
        )
        return(
            <div className="scan-history-box" {...this.props}>
                <HeaderDropdown
                    placement="bottomLeft"
                    overlay={scanList}
                    overlayClassName={ collapsed ? "scan-history-tabs-collapsed" : "scan-history-tabs"}
                    trigger={['click']}
                    visible={this.state.visible}
                    onVisibleChange={this.handleVisibleChange}
                    {...this.props}
                    >
                    <div>
                        <Icon type="history" />
                    </div>
                </HeaderDropdown>
            </div>
        )
    }
}