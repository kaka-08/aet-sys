import React from 'react';
import { List,Tag, Icon } from 'antd';
import './index.less';

export default function NoticeList({}){
   
    const data = [
        {
            desc: '康师傅要过期了',
            tag: '即将过期'
        },
        {
            desc: '康师傅距离过期还有5天,康师傅存货严重不足',
            tag: '库存不足'
        },
        {
            desc: '康师傅存货严重不足，已经报警,康师傅存货严重不足',
            tag: '已经过期'
        },
        {
            desc: '目前未结算订单较多，请及时清除',
            tag: '订单过多'
        },
      ];
    return (
    <List
        className="notice-list"
        itemLayout="horizontal"
        dataSource={data}
        renderItem={item => (
        <List.Item extra={
            <div className="notice-icon">
                <div className="icon"><Icon type="user" /></div>
                <div className="desc">{item.desc}</div>
        <span className="notice-tag"><Tag color="magenta">{item.tag}</Tag></span>
            </div>
        }>
            {/* <List.Item.Meta
                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title={<a href="https://ant.design">{item.title}</a>}
                description="Ant Design, a design language for background applications, is refined by Ant UED Team"
            /> */}
        </List.Item>
        )}
    />
)
}