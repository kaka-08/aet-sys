import React, { Component } from 'react';
import { Tabs,Badge, Icon, Button } from 'antd';
import HeaderDropdown from '../components/HeaderDropdown';
import NoticeList from './components/NoticeList';
import './index.less'

const { TabPane } = Tabs;
class NoticeIconView extends Component {

  state={
    visible: false
  }

  handleVisibleChange = flag => {
    this.setState({ visible: flag });
  };


  render(){
    const noticeBox = (
      <>
        <Tabs defaultActiveKey="notice" onChange={null}>
          <TabPane tab="通知（4）" key="notice">
            <NoticeList/>
          </TabPane>
          <TabPane tab="待办（3）" key="todo">
            <NoticeList/>
          </TabPane>
          <TabPane tab="消息（4）" key="message">
            <NoticeList/>
          </TabPane>
        </Tabs>
        <div className="bottom-bar">
          <div>加载更多</div>
          <div>清除所有</div>
        </div>
      </>
    );
    return(
      <div className={"notice-view"} {...this.props}>
        <HeaderDropdown
          placement="bottomRight"
          overlay={noticeBox}
          overlayClassName={"notice-box"}
          trigger={['click']}
          visible={this.state.visible}
          onVisibleChange={this.handleVisibleChange}
          {...this.props}
        >
          <div>
            <Badge dot>
              <Icon style={{fontSize:16}} type="bell" />
            </Badge>
          </div>
        </HeaderDropdown>
      </div>
    )
  }
}

export default NoticeIconView;
