import React from 'react'
import { Menu,Icon } from 'antd';
import './index.less'
import menu_list from '../../constants/navLeft';
import { connect } from 'react-redux';
const SubMenu = Menu.SubMenu;
class NavLeft extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            menuTreeNode: null
        }
    }

    componentDidMount(){
        this.setState({ menuTreeNode: this.renderMenu(menu_list) });
        let path = this.props.history.location.pathname;
        if(path === '/'){
            this.props.updateKeys('','');
        }
    }

    handleMenuClick=(e,item)=>{
        let { history } = this.props;
        this.props.updateHistory(item);
        this.props.updateKeys(item.parent_id,item.menu_id);
        history.push(item.url);
    }

    // 菜单渲染
    renderMenu = (data) => {
        return data && data.map(item=>{
            if(item.children && item.children.length){
                return (
                    <SubMenu title={
                        <span>
                            <Icon type={item.icon} />
                            <span>{item.name}</span>
                        </span>
                        } 
                        key={item.menu_id}>
                        { this.renderMenu(item.children) }
                    </SubMenu>
                )
            }else{
                return <Menu.Item 
                    title={item.name} 
                    key={item.menu_id} 
                    onClick={(e)=>this.handleMenuClick(e,item)}>
                    {/* {item.name} */}
                    <span>
                        <Icon type={item.icon} />
                        <span>{item.name}</span>
                    </span>
                </Menu.Item>
            }
        })
    }
    render() {
        const { collapsed,defaultOpenKeys,selectedKeys } = this.props;
        return (
            <div className="nav-left">
                <a className="logo">
                    <img alt="logo" src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/aws.png" />
                    {
                        !collapsed 
                        &&
                        <h1>熊猫仓库</h1>
                    }
                </a>
                <Menu
                    className="menu-box"
                    theme="dark"
                    mode="vertical"
                    defaultSelectedKeys={[selectedKeys]}
                    defaultOpenKeys={[defaultOpenKeys]}
                >
                    { this.state.menuTreeNode }
                </Menu>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { scanHistory } = state;
    return { 
        selectedKeys: scanHistory.current_key,
        defaultOpenKeys: state.parent_key,
    }
}


const mapDispatchToProps = dispatch => {
    return {
        updateHistory: (record) => dispatch({ 
            type:"scan_history/update",
            payload: record
        }),
        updateKeys: (parent_key,current_key) => dispatch({ 
            type:"scan_history/updateKeys",
            payload: {
                current_key,
                parent_key
            }
        }),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(NavLeft)