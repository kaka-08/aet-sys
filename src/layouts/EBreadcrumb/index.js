import React,{Component}  from 'react'
import { Breadcrumb, Icon } from 'antd';
import keysMap from '../../constants/routeKeysMap';
import ignoreKeysMap from '../../constants/ignoreKeysMap';
import './index.less'

class EBreadcrumb extends Component{ 

    title = ""
    
    shouldComponentUpdate(nextProps,nextState){
        if(this.props.location.pathname === window.location.hash.substr(1)){
            return false
        }
        return true
    }

    renderBreadcrumbItems=()=>{
        const { pathname } = this.props.location;
        let result = pathname.split('/');
        const target = this.assembleFinalKeys(result);
        return target.map((item,index)=><Breadcrumb.Item key={index}>
                {item === "" && <Icon type="home" />}<span>{keysMap.get(item)}</span>
            </Breadcrumb.Item>
        )
    }

    /**
     * source = ['','index','a','b']
     * target = ['/','/index','/index/a','/index/a/b']
     * 待优化 
     */
    assembleFinalKeys=(source)=>{
        //数组去重复
        source = Array.from(new Set(source));
        let target = [];
        for(let i=0;i<source.length;i++){   
            let _str = "";
            for(let j=1;j<=i;j++){
                _str += "/"+source[j]
            }
            target.push(_str);
        }
        return target;
    }


    //检索被忽略的路由，避免出现 空白面包屑的显示 eg: 首页/CICD/ / / 
    checkIsIgnore=()=>{
        const { pathname } = this.props.location;
        let result = false;

        for (let [key, value] of ignoreKeysMap) {
            if(pathname.indexOf(key) > -1) {
                result = true;
                this.title = value;  
                break;
            }
        }
        return result;
    }


    render(){
        const isIgnore = this.checkIsIgnore();
        return(
            <div className="e-breadcrumb-container">
                {
                    isIgnore
                    ?
                    <CommonTitle title={this.title} />
                    :
                    <>
                        <Breadcrumb >
                            { this.renderBreadcrumbItems() }
                        </Breadcrumb>
                        <h1>高级表单</h1>
                        <p>高级表单常见于一次性输入和提交大批量数据的场景。</p>
                    </>
                }
            </div>
        )
    }
}

export default EBreadcrumb


export function CommonTitle({title}){
    return <div className="common-title">{title}</div>
}