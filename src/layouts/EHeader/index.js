import React from 'react'
import { Button,Icon } from "antd"
import Store from "utils/Store";
import { logoutService } from 'service/login-out';
import GlobalHeaderRight from '../GlobalHeaderRight';
import './index.less'


let timer = null;

 class EHeader extends React.Component{
    state={
        username: Store.fetch('username') ? Store.fetch('username') : ''
    }

    toLogin=()=>{
        this.props.history.push('/login');
    }

    logout = () => {
        logoutService().then(res=>{
            Store.clear();
            setTimeout(()=>{
                this.props.history.push('/login');
            },300)
        })
    };

    componentWillUnmount(){
        clearInterval(timer)
    }

    render(){
        return (
            <div className="header" {...this.props}>
                <Button onClick={this.props.toggleCollapsed} className="collapsed-btn">
                    <Icon type={this.props.collapsed ? 'menu-unfold' : 'menu-fold'} />
                </Button>
                <GlobalHeaderRight collapsed={this.props.collapsed} />
            </div>
        );
    }
}

export default EHeader
