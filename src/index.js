import React from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import Routers from './routers';
import { ConfigProvider } from 'antd';
import { Provider } from 'react-redux';
import store from './store';
import zhCN from 'antd/es/locale/zh_CN';
import 'moment/locale/zh-cn';

var containerRoot = document.getElementById('root');
debugger;
console.log('containerRoot %o',containerRoot);
const render = () => {
    ReactDOM.render(
      <ConfigProvider locale={zhCN}>
        <Provider store={store}>
          <Routers />
        </Provider>
      </ConfigProvider>, document.getElementById('root')
    );
  }

render();

// 热更新
if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('./routers', render)
  }

