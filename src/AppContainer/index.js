import React, { Component, useState } from 'react';
import { Layout } from 'antd';
import EHeader from '../layouts/EHeader'
import NavLeft from '../layouts/NavLeft'
import EBreadcrumb from '../layouts/EBreadcrumb';
import Store from 'utils/Store';
import { withRouter } from 'react-router-dom'; 
import './index.less'

const { Header, Content, Footer, Sider } = Layout;

const AppContainer =(props={})=>{

  const [collapsed,setCollapsed] = useState(false);

  const toggleCollapsed = () => {
    setCollapsed(!collapsed)
  };

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <NavLeft 
          history={props.history} 
          {...props} 
          collapsed={collapsed} 
        />
      </Sider>
      <Layout>
        <Header>
          <EHeader 
            history={props.history} 
            location={props.location} 
            collapsed={collapsed} 
            toggleCollapsed={toggleCollapsed} 
          />
        </Header>
        <Content className="container">
            <EBreadcrumb history={props.history} location={props.location}/>
            <div className="main-container">{props.children}</div>
        </Content>
        <Footer className="footer">版权所有：AWMC（推荐使用谷歌浏览器以获得更快页面响应速度） 技术支持：awmc团队</Footer>
      </Layout>
    </Layout>
  );
}


export default withRouter(AppContainer);
