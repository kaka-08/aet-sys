import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
// We'll use redux-logger just as an example of adding another middleware
import logger from 'redux-logger'
import reducer from './reducer'

const middleware = [...getDefaultMiddleware(), logger];

// The store has been created with these options:
// - The slice reducers were automatically passed to combineReducers() 这里很重要。。。无需手动合并
// - redux-thunk and redux-logger were added as middleware
// - The Redux DevTools Extension is disabled for production
// - The middleware, and devtools enhancers were automatically composed together
const store = configureStore({
    reducer,
    middleware,
    devTools: process.env.NODE_ENV !== 'production',
    enhancers: []
})

if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./reducer', () => store.replaceReducer(reducer))
}

export default store;