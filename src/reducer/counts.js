import { createSlice  } from '@reduxjs/toolkit'

const counts = createSlice({
    name: 'counts',
    initialState: {
        count: 10
    },
    reducers: {
        increment(state, action) {
            return {
                // count: state.count + action.count
                count: state.count
            }
        },
        decrement(state, action) {
            return {
                count: state.count - action.count
            }
        }
    }
  })

  export default counts