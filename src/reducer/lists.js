import { createSlice  } from '@reduxjs/toolkit'

const lists = createSlice({
    name: 'lists',
    initialState: {
        names:[]
    },
    reducers: {
        add(state, action) {
            return {
                names: [...state.names,action.name]
            }
        }
    }
  })

  export default lists