import { createSlice  } from '@reduxjs/toolkit'

const global = createSlice({
    name: 'global',
    initialState: {
        loading: false
    },
    reducers: {
        loading(state, action) {
            return {
                loading: true
            }
        },
        finished(state, action) {
            return {
                loading: false
            }
        }
    }
  })

  export default global