import { createSlice  } from '@reduxjs/toolkit'
import Store from 'utils/Store';
import menu_list from '../constants/navLeft';

/**
 * 
 * @param {object} record 子菜单数据 
 */
function updateHistory(record){
    const lists = Store.fetch('scan_history') || [];
    // 1: 缓存当中是否已经有一级菜单
    let hasIndex = lists.findIndex(item=>item.menu_id === record.parent_id);
    let parentIndex = menu_list.findIndex(item=>item.menu_id === record.parent_id);
    if( hasIndex === -1){
        //1.1: 缓存当中无一级菜单
        let temp = menu_list[parentIndex];
        lists.push(Object.assign({
            ...temp,
            children:[record]
        }))
    }else{
        let isExistIndex = lists[hasIndex].children.findIndex(item=>item.menu_id === record.menu_id);
        if(isExistIndex === -1){
            lists[hasIndex].children.push(record)
        }
    }
    Store.save('scan_history',lists)
    return lists
}


const scanHistory = createSlice({
    name: 'scan_history',
    initialState: {
        list: Store.fetch('scan_history') || [],
        current_key: Store.fetch('current_key') || '',
        parent_key: Store.fetch('parent_key') || ''
    },
    reducers: {
        lists(state) {
            return {
                list: state.list
            }
        },
        clear(state,action){
            Store.save('scan_history',[]);
            state.list = []
        },
        update(state,action){
            let lists = updateHistory(action.payload);
            state.list = lists
        },
        updateKeys(state,action){
            state.current_key = action.payload.current_key;
            state.parent_key = action.payload.parent_key;
            Store.save("current_key",action.payload.current_key);
            Store.save("parent_key",action.payload.parent_key);
        },
    }
  })

  export default scanHistory