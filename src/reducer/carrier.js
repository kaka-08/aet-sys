import { createSlice  } from '@reduxjs/toolkit'

const carrier = createSlice({
    name: 'carrier',
    initialState: {
        list: [],
    },
    reducers: {
        updateCarriers(state, action) {
            state.list = action.payload
        }
    }
  })

  export default carrier