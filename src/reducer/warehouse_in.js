import { createSlice  } from '@reduxjs/toolkit'

const warehouseIn = createSlice({
    name: 'warehouse_in',
    initialState: {
        skus: [],
        orderInfo: {}
    },
    reducers: {
        updateSkus(state, action) {
            state.skus = action.payload
        },
        updateOrderInfo(state, action) {
            state.orderInfo = action.payload
        },
        clearData(state,action){
            return {
                skus: [],
                orderInfo: {} 
            }
        }
    }
  })

  export default warehouseIn