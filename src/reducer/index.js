import counts from './counts';
import lists from './lists';
import global from './global';
import scanHistory from './scan_history';
import warehouseIn from './warehouse_in';
import carrier from './carrier';


const reducer = {
    counts: counts.reducer,
    lists: lists.reducer,
    global:global.reducer,
    scanHistory:scanHistory.reducer,
    warehouseIn:warehouseIn.reducer,
    carrier:carrier.reducer,
}

export default reducer;