import {Modal,Select,Icon} from 'antd'
import React from 'react'
import Store from './Store'
import { withRouter } from "react-router";
import moment from 'moment';
const Option = Select.Option;

let UtilFun = {
    getCookie(name) {
        var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
        if (arr = document.cookie.match(reg))
            return decodeURIComponent(arr[2]);
        else
            return null;
    },
    setCookie(name, value){
        var Days = 30;
        var exp = new Date();
        exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
        document.cookie = name + "=" + encodeURIComponent(value) + ";expires=" + exp.toGMTString();
    },
    clearUid(){
        let date = new Date();
        date.setTime(date.getTime() - 24 * 60 * 60 * 60);
        document.cookie = 'Server-Token=;domain=songguo7.com;expires=' + date.toGMTString();
    },
    sleep(d){ //利用for循环实现sleep
        for(let t = Date.now();Date.now() - t <= d;);
    },
    trim(content){
        if(typeof content == 'string'){
            return content.replace(/^\s*|\s*$/g,'');
        }
        return content;
    },
    //统计字节长度
    countByteLength(str){
        var byteLen = 0;
        for (var i = 0; i < str.length; i++) {
            if (/[\x00-\xff]/g.test(str.charAt(i))) {
                byteLen += 1;
            } else {
                byteLen += 2;
            }
        }
        return byteLen;
    },
    fixAmount(text) {
        return (text / 100).toFixed(2)
    },
    fixEmptyRichText(value) {
        if (/img|video/i.test(value)) {
            return value
        }
        if (value.replace(/<[^>]+>|&nbsp;|\s+/g, '') == '') {
            return ''
        }
        return value
    },
    fixNum(num) {
        if (num < 10) {
            num = '0' + num
        }
        return num.toString();
    },
    toCent(){
        let val = Number(this);
        if (isNaN(val) || !val) {
            return 0;
        }
        val += '';
        let arr = val.split(".");
        if (arr.length == 1) {
            return arr[0] * 100;
        } else if (arr.length == 2) {
            //如果长度为1位,末位补0
            if (arr[1] && arr[1].length == 1) {
                arr[1] = arr[1] + '0';
            }
            //如果超过2位,直接舍弃
            if (arr[1] && arr[1].length > 2) {
                arr[1] = arr[1].substr(0, 2);
            }
            return Number(arr.join(""));
        }
    },
    // 格式化秒
    formatTime(date){
        if (!date) {
            return '';
        }
        let time = new Date(date).getTime().toString();
        if(time == 'NaN')return date;
        return parseInt(time);
    },
    // 格式化日期(eg:2017-10-08 15:00:00)
    formatDate(timestamp) {
        if (!timestamp) {
            return '';
        }
        // 兼容新旧时间戳，如果后台返回的是秒，则补000，转换为毫秒
        if (timestamp.toString().length==10){
            timestamp += '000';
        }
        var date = new Date(+timestamp);
        return [
                date.getFullYear(),
                UtilFun.fixNum(date.getMonth() + 1),
                UtilFun.fixNum(date.getDate())
            ].join('-') + ' ' + [
                UtilFun.fixNum(date.getHours()),
                UtilFun.fixNum(date.getMinutes()),
                UtilFun.fixNum(date.getSeconds())
            ].join(':')
    },
    // 格式化日期(eg:2017-10-08)
    formatShortDate(timestamp) {
        if (!timestamp) {
            return '';
        }
        // 兼容新旧时间戳，如果后台返回的是秒，则补000，转换为毫秒
        if (timestamp.toString().length == 10) {
            timestamp += '000';
        }
        var date = new Date(+timestamp);
        return [
            date.getFullYear(),
            UtilFun.fixNum(date.getMonth() + 1),
            UtilFun.fixNum(date.getDate())
        ].join('-')
    },
    // 格式化时间(eg:1小时15分钟20秒)
    formatDuration(timestamp) {
        if (!timestamp) {
            return '';
        }
        return parseInt(timestamp / 60 / 60) + "小时" + parseInt(timestamp / 60 % 60) + "分钟" + parseInt(timestamp % 60 % 60) + "秒"
    },
    // 将日期格式化为时间戳-秒
    formatTimestamp(dateStr){
        let time = new Date(dateStr).getTime().toString();
        return parseInt(time.substr(0, time.length - 3));
    },
    // 时间区间判断
    betweenDays(begin_time, end_time, days = 3){
        if (begin_time) {
            // 此处需要判断当前时间是否为秒单位，否则才需要进行日期转换
            if (typeof begin_time == 'number'){
                begin_time *= 1000;
            }else{
                begin_time = new Date(begin_time).getTime();
            }
        } else {
            begin_time = new Date().getTime();
        }
        if (end_time) {
            if (typeof end_time == 'number') {
                end_time *= 1000;
            }else{
                end_time = new Date(end_time).getTime();
            }
        } else {
            end_time = new Date().getTime();
        }
        if (end_time - begin_time > days * 24 * 60 * 60 * 1000) {
            return false;
        }
        return true;
    },
    // 格式化金额,单位:分(eg:430分=4.30元)
    formatFee(fee, suffix = '') {
        if (!fee) {
            return 0;
        }
        if (typeof suffix != "string") {
            suffix = ""
        }
        fee = Math.round(fee) / 100;
        var s = fee.toString();
        var rs = s.indexOf('.');
        if (rs < 0) {
            rs = s.length;
            s += '.';
        }
        while (s.length <= rs + 2) {
            s += '0';
        }
        return s + suffix;
    },
    // 格式化金额（加分位符 ,）,单位: 分(eg:456789分 =  4,567.89元  )
    formatMoney(s, n = 2) {
        if (s === 0) {
            return "0.00"
        }
        if (!s) {
            return ""
        }
        s = s.toFixed(0) / 100;
        s = s.toLocaleString();
        if (s.indexOf(".") > -1) {
            let f = s.split(".")[1];
            if (f.length == 1) {
                s += "0"
            }
        } else {
            s += ".00"
        }
        return s;
    },
    // 格式化公里（eg:3000 = 3公里） fixed: km保留的小数位数
    formatMileage(mileage, text, fixed) {
        if (!mileage) {
            return 0;
        }
        if (mileage >= 1000) {
            text = text || " km";
            if (fixed && fixed>=1 && fixed <=3) {
                return Math.floor(mileage / Math.pow(10, 3 - fixed) ) / Math.pow(10, fixed)  + text;
            }
            return Math.floor(mileage / 100) / 10 + text;
        } else {
            text = text || " m";
            return mileage + text;
        }
    },
    // 隐藏手机号中间4位
    formatPhone(phone) {
        if (!phone) {
            return ""
        }
        phone += '';
        return phone.replace(/(\d{3})\d*(\d{4})/g, '$1****$2')
    },
    /*
     * 分页代码封装
     * @useage:
     * Utils.pagination(data,(current)=>{
     *    this.params.page = current;
     *    this.requestList()
     * })
     * */
    pagination(data, callback){
        if(typeof data.result === 'undefined'){
            data.result = data;
        }
        var page = {
            onChange: (current) => {
                callback && callback(current);
            }
        };
        // 如果是requestList接口会返回page对象，如果是弹框里面的list不会包含page对象，需要单独取值
        let total = data.page.total || data.total_count;
        page.current = typeof data.page === 'object' ? data.page.page : data.page;
        page.pageSize = data.page.pageSize || data.page_size;
        page.total = total;
        page.showTotal = () => {
            return '共' + total + '条 '
        };
        page.showQuickJumper = true;
        return page;
    },
    goToDownload(url){
        if (url.indexOf('/data-report') > -1){
            url = url.replace('backend', 'data-report');
        }
        window.open(url, "_blank");
    },
    
    //根据列表生成一个标注的List，用于生成下拉框组件使用
    createOptionList(data,isDelAll,keyOption = {}){
        if (!data) {
            return [];
        }
        let list = [{ id: '', name: '全部' }];
        data.map((item)=> {
            if (typeof keyOption.id != 'undefined') {
                item.id = item[keyOption.id];
                item.name = item[keyOption.name];
            }
            list.push({
                id:item.id,
                name:item.name
            });
        })
        //是否删除全部选项，true:删除，默认显示
        if(isDelAll){
            list.splice(0, 1)
        }
        return list;
    },
    // 根据data生成Option List
    getOptionList(data, isDelAll, keyOption = {}){
        if (!Array.isArray(data)) {
            return [];
        }
        var city_options = [<Option value="" key="">全部</Option>];
        data.map((item)=> {
            if (typeof keyOption.id != 'undefined') {
                item.id = item[keyOption.id];
                item.name = item[keyOption.name];
            }
            var option = '';
            if (keyOption.isDisabled && item.is_assigned){
                option = <Option value={item.id} key={item.id + ''} style={{color:'red'}}>{item.name+'(已分配)'}</Option>;
            }else{
                option = <Option value={item.id} key={item.id + ''}>{item.name}</Option>;
            }

            city_options.push(option);
        })
        //是否删除全部选项，true:删除，默认显示
        if(isDelAll){
            city_options.splice(0, 1)
        }
        return city_options;
    },

    //图片或者文件上传功能封装
    fileUploadConfig(_this, type, options={}){
        return {
            action: options.action,//默认使用通用的文件上传
            name: "fileData",
            listType: options.listType || "picture-card",
            className: options.className || "",
            headers: {"Authorization": 'Bearer ' + localStorage.token},
            fileList: _this.state.fileList || [],
            showUploadList: options.showUploadList || false,
            onChange(info){
                let file = info.file;
                if (file.response && file.response.error) {
                    UtilFun.ui.alert({text: file.response.error.message});
                    return;
                }
                let fileList = info.fileList;
                //只显示最近一个上传的文件
                fileList = fileList.slice(-1);//从后面截取一个元素
                // 2. read from response and show file link
                fileList = fileList.map((file) => {
                    if (file.response) {
                        // Component will show file.url as link
                        file.url = file.response.result.url;
                    }
                    return file;
                });
                if (fileList.length>0){
                    if (file.response && file.response.result && file.response.result.url) {

                        _this.props.form.setFieldsValue({
                            [type]: file.response.result.url
                        });
                    }
                }

                _this.setState({
                    fileList: fileList,
                    [options.fileListParam]: fileList
                });
            },
            onRemove(){
                _this.props.form.setFieldsValue({
                    [type]: ''
                });
                _this.setState({
                    fileList: []
                });
            }
        };
    },
    checkBtnPermission(_this, curPage){
        let btnKeys = Store.fetch("btnKeys");//已开启的权限
        let router_key = window.location.href.split(/[#?]/)[1];
        if (/\/\d*$/.test(router_key)) {
            router_key = router_key.replace(/\/\d*$/g, '');
        }
        if (curPage) {
            router_key = curPage;
        }
        //过滤当前页面按钮的权限
        let curPageBtn = btnKeys && btnKeys.filter((item)=> {
                if (curPage && item.indexOf(curPage) > -1) {
                    return item;
                } else if (item.indexOf(router_key) > -1) {
                    return item
                }
            }) || '';
        _this.setState({
            routerKey: router_key,
            curPageBtn: curPageBtn
        });
    },
    /**
     * ETable 行点击通用函数
     * @param {*选中行的索引} selectedRowKeys
     * @param {*选中行对象} selectedItem
     */
    updateSelectedItem(selectedRowKeys, selectedRows, selectedIds){
        if (selectedIds) {
            this.setState({
                selectedRowKeys,
                selectedIds: selectedIds,
                selectedItem: selectedRows
            })
        } else {
            this.setState({
                selectedRowKeys,
                selectedItem: selectedRows
            })
        }
    },
    // 获取默认城市ID
    getDefCity(){
        return Store.fetch("defaultCity") || ''
    },
    // 普通路由跳转
    goToPage(route){
        if (route[0] != "/") {
            return;
        }
        this.props.history.push(route)
    },
    goBlankPage(url){
        window.open(url, "_blank");
    },
    // 当前页面权限按钮过滤
    filterBtnKeys(btnKeys){
        if (!btnKeys) {
            return {}
        } else if (!btnKeys.filter) {
            return {}
        } else {
            const router_key = window.location.hash.split("#")[1];
            //过滤当前页面按钮的权限
            return btnKeys.filter((item)=> {
                if (item.indexOf(router_key) > -1) {
                    return item
                }
            });
        }
    },
    // 首页模块 权限过滤
    filterHomeBtnKeys(btnKeys){
        if (!btnKeys) {
            return {}
        } else if (!btnKeys.filter) {
            return {}
        } else {
            const router_key = [
                '/maintenance/warning',
                '/maintenance/monitor/monitor_bike',
                '/maintenance/faulty/waiting_repair',
                '/order'
            ]

            //过滤当前页面按钮的权限
            return btnKeys.filter((item)=> {
                if (item.indexOf(router_key[0]) > -1) {
                    return item
                } else if (item.indexOf(router_key[1]) > -1) {
                    return item
                } else if (item.indexOf(router_key[2]) > -1) {
                    return item
                } else if (item.indexOf(router_key[3]) > -1) {
                    return item
                }
            });
        }
    },
    // 判断按钮是否有权限
    checkBtnPerm(btnKeysList, btnIndex, routerKey){
        const router_key = routerKey || window.location.hash.split("#")[1];
        if (btnKeysList && btnKeysList.indexOf) {
            return btnKeysList.indexOf(router_key + '-btn' + btnIndex) > -1 || btnKeysList.indexOf('#' + router_key + '-btn' + btnIndex) > -1;
        }
    },
    //对象深度复制
    extend(obj){
        if (typeof obj != 'object') {
            return obj;
        }
        var newobj = {};
        for (var attr in obj) {
            newobj[attr] = this.extend(obj[attr]);
        }
        return newobj;
    },
    //活动系统中，金额、天数、次数需要转换为数字
    stringToInt(coupon_products){
        for(let k in coupon_products){
            if(typeof coupon_products[k] != 'object'){
                //获取每一个值，判断是否为数字
                let val = parseFloat(coupon_products[k]*1);
                if(!isNaN(val)){
                    coupon_products[k] = val;
                }
            }
        }
    },
    /**
     * 转换参数
     * @param { Object }
     * @return { Object }
     * @description 深拷贝 + 检测number类型
     */
    adaptJson(source) {
        var result = Array.isArray(source) ? [] : {};
        for (var key in source) {
            if (source.hasOwnProperty(key)) {
                if (typeof source[key] === 'object') {
                    result[key] = this.adaptJson(source[key]);
                } else {
                    if(!isNaN(source[key]*1)){
                        result[key] = source[key] *1;
                    }else if(!source[key]){
                        delete source[key]
                    }else if(typeof source[key] === 'string'){
                        result[key] = source[key].trim();
                    }else{
                        result[key] = source[key]
                    }
                }
            }
        }
        return result
    },
    // 删除值为空的字段
    delEmptyParam(params){
        for(let key in params){
            if (!params[key]) {
                delete params[key];
            }
        }
    },
    // 查看时显示字段不显示组件
    viewComponent(Cmp) {
        return class WrapCmp extends React.Component {
            render () {
                if (this.props.isView) return this.props.viewField;
                return <Cmp history={this.props.history}/>;
            }
        }
    },
    /**
     * 获取 blob
     * @param  {String} url 目标文件地址
     * @return {Promise}
     */
    getBlob(url) {
        return new Promise(resolve => {
            const xhr = new XMLHttpRequest();

            xhr.open('GET', url, true);
            xhr.responseType = 'blob';
            xhr.onload = () => {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                }
            };

            xhr.send();
        });
    },

    /**
     * 保存
     * @param  {Blob} blob
     * @param  {String} filename 想要保存的文件名称
     */
    saveAs(blob, filename) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');
            const body = document.querySelector('body');

            // link.href = window.URL.createObjectURL(blob);
            link.href = window.URL.createObjectURL(new Blob([blob], {type: 'application/msword'}));
            link.download = filename;

            // fix Firefox
            link.style.display = 'none';
            body.appendChild(link);

            link.click();
            body.removeChild(link);

            window.URL.revokeObjectURL(link.href);
        }
    },

    /**
     * 处理时间差
     * @param {*} before 
     * @param {*} after 
     * @param {*} ftm  default = "YYYY-MM-DD hh:mm:ss"
     */
    timeDiff(before,after,ftm="YYYY-MM-DD HH:mm:ss"){
        let _start = Date.parse(new Date(before));
        let _end =  Date.parse(new Date(after !== undefined ?  after : moment().format(ftm)));
        let diffTime = (_end - _start)/1000;  // 秒
        let diffDay = diffTime / ( 60 * 60 * 24);
        let diffHour = (diffDay -  Math.floor(diffDay))*24;
        let diffMinute = (diffTime - Math.floor(diffDay)*24*60*60 - Math.floor(diffHour)*60*60)/60; 
        return `${Math.floor(diffDay)}天${Math.floor(diffHour)}小时${Math.floor(diffMinute)}分`;
    },
    /**
     * 下载
     * @param  {String} url 目标文件地址
     * @param  {String} filename 想要保存的文件名称
     */
    downloadReName(url, filename) {
        this.getBlob(url).then(blob => {
            this.saveAs(blob, filename);
        });
    }
};
export default withRouter(UtilFun);
