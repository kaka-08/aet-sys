const STORAGE_KEY = 'awmc';
export default {
    fetch(key){
        let storage = this.getStorage();
        return storage[key];
    },
    save(key, val){
        let storage = this.getStorage();
        storage[key] = val;
        window.localStorage.setItem(STORAGE_KEY, JSON.stringify(storage))
    },
    clear(){
        window.localStorage.setItem(STORAGE_KEY, "")
    },
    getStorage(){
        return JSON.parse(window.localStorage.getItem(STORAGE_KEY) || '{}');
    }
}
