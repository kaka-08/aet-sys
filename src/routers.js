import React, { Suspense, lazy } from 'react';
import {
  HashRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

//loading
import Loading from './components/Loading';
//layouts
import AppContainer from './AppContainer';

//login
const Login = lazy(() => import(/* webpackChunkName: "login" */'./pages/login'));

//main page
const Home = lazy(() => import(/* webpackChunkName: "home" */'./pages/home'));
const ListDemo = lazy(() => import(/* webpackChunkName: "home1" */'./pages/list'));
const ListDemo1 = lazy(() => import(/* webpackChunkName: "home1" */'./pages/detail'));
const Brand = lazy(() => import(/* webpackChunkName: "home1" */'./pages/res/brand'));
const Category = lazy(() => import(/* webpackChunkName: "home1" */'./pages/res/category'));
const Carrier = lazy(() => import(/* webpackChunkName: "carrier" */'./pages/third-party/supplier/carrier'));
const Supplier = lazy(() => import(/* webpackChunkName: "supplier" */'./pages/third-party/supplier/supplier'));
const Sku = lazy(() => import(/* webpackChunkName: "sku" */'./pages/res/sku/sku'));
const SkuTpl = lazy(() => import(/* webpackChunkName: "sku-tpl" */'./pages/res/sku/tpl'));
const NewTpl = lazy(() => import(/* webpackChunkName: "tpl" */'./pages/res/sku/tpl/new-tpl'));
const Goods = lazy(() => import(/* webpackChunkName: "goods" */'./pages/res/goods'));
const GoodsInfo = lazy(() => import(/* webpackChunkName: "goods-info" */'./pages/res/goods/info'));

const In = lazy(() => import(/* webpackChunkName: "in" */'./pages/warehouse/in'));
const NewIn = lazy(() => import(/* webpackChunkName: "new-in" */'./pages/warehouse/new-in'));
const CurrentStock = lazy(() => import(/* webpackChunkName: "now" */'./pages/warehouse/now/list'));

const Pending = lazy(() => import(/* webpackChunkName: "new-in" */'./pages/pending'));


export default function Routers() {
    return (
      <Router>
          <Suspense fallback={<Loading />}>
            <Switch>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/detail" component={ListDemo1}/>
              <Route path="/res/goods-info/:id/:tpl_id" component={GoodsInfo} />
              <Route path="/" render={()=>
                <AppContainer>
                  <Route exact path="/" component={Home}/>
                  <Route path="/list" component={ListDemo}></Route>
                  <Route path="/user" component={null}></Route>
                  <Route path="/res/brand" component={Brand}></Route>
                  <Route path="/res/category" component={Category}></Route>
                  <Route path="/res/goods" component={Goods}></Route>
                  <Route path="/res/sku/list" component={Sku}></Route>
                  <Route path="/res/sku/tpl" component={SkuTpl}></Route>
                  <Route path="/res/sku/tpl-edit/:id" component={NewTpl}></Route>
                  <Route path="/third-party/supplier/carrier" component={Carrier}></Route>
                  <Route path="/third-party/supplier/list" component={Supplier}></Route>
                  <Route path="/warehouse/in" component={In}></Route>
                  <Route path="/warehouse/new-in" component={NewIn}></Route>
                  <Route path="/warehouse/now/list" component={CurrentStock}></Route>
                  <Route path="/pending/new-in" component={NewIn}></Route>
                  {/* <Route path="*"><Pending /></Route> */}
              </AppContainer>
              }/>
          </Switch>
        </Suspense>
      </Router>
    );
  }
  