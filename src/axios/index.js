import qs from 'qs';
import axios from 'axios';
import { notification,Modal } from 'antd';
import { base } from 'config/defaults';
import Store from 'utils/Store';
// import store from '../store';

//基本的配置
const baseCfg  = {
    baseURL: [window.location.protocol,window.location.host].join('//'), 
    timeout: base.timeout,
    withCredentials:true, //携带cookie信息，default false
    // paramsSerializer: function (params) {
    //   return qs.stringify(params, {arrayFormat: 'brackets'})
    // }
};

const axios_instance = axios.create(baseCfg);

// Interceptors
// Do something before request is sent
// axios_instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
// axios.defaults.headers.post['content-Type'] = 'appliction/x-www-form-urlencoded';
axios_instance.interceptors.request.use(function (config) {
  // if(config.method === 'post'){
  //   axios_instance.defaults.headers.post['Content-Type'] = 'application/json';
  //   // axios_instance.he
  //   // headers: { 'content-type': 'application/x-www-form-urlencoded' },
  // }
    // store.dispatch({ type: 'global/loading'})  //需要时候再去放开，目前放开并没有什么好的体验 2020-02-19 
    //查看缓存有无token，有，则携带
    if(Store.fetch("token")){
      config.headers.common['token'] = Store.fetch("token");
    }else{
      // window.location.href = window.location.protocol + '//'+ window.location.hostname + '/#/login';
    }
    return config;
  }, function (error) {
    // Do something with request error  
    return Promise.reject(error);
  });
 
//添加响应的拦截器
axios_instance.interceptors.response.use(function (response) {
    //store.dispatch({ type: 'global/finished'}) //需要时候再去放开，目前放开并没有什么好的体验 2020-02-19 
    if(response.headers.token){
      //如果返回token，则将授权信息保存在axios之后的请求当中 并且保存在浏览器当中，防止刷新axios重新实例化时候丢失
      axios_instance.defaults.headers.common['token'] = response.headers.token;
      Store.save("token",response.headers.token);
    }
    // 状态码正常的响应
    if(response.data.code === 0){
      return response.data.data;
    }else{
        //code 非0 业务异常
        if(response.data.code === 10001){
          notification.error({
            message: response.data.msg,
            description: `[系统提示]：${response.data.data}`
          });
        }else{
          notification.error({
            message: "操作异常",
            description: `[系统提示]：${response.data.msg}`
          });
        }
    }
  }, 
  function (error) {
    Modal.error({
      title: `系统异常：${error.response.status}`,
      content: `[异常信息]：${error.message}，请您及时联系管理员处理！`
    });
    // 网络异常
    return Promise.reject(error)
  });


export default axios_instance;
