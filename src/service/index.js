import axios_instance from '../axios';

export function queryEnvService(params){
    return axios_instance.get('/api/cmdb/v1/env',{params})
}

//商标
export function brandsService(params){
    return axios_instance.post('/v1/aet/backend/brand/list',params)
}

export function brandAddService(params){
    return axios_instance.post('/v1/aet/backend/brand/add',params)
}

export function brandUpdateService(params){
    return axios_instance.post('/v1/aet/backend/brand/update',params)
}

//承运人
export function carriersService(params){
    return axios_instance.post('/v1/aet/backend/carrier/list',params)
}

export function carrierAddService(params){
    return axios_instance.post('/v1/aet/backend/carrier/add',params)
}


export function goodsService(params){
    return axios_instance.post('/v1/aet/backend/goods/list',params)
}

export function goodsAddService(params){
    return axios_instance.post('/v1/aet/backend/goods/add',params)
}

export function goodsDetailService(id){
    return axios_instance.post(`/v1/aet/backend/goods/detail/${id}`)
}

export function goodsBatchService(id){
    return axios_instance.post(`/v1/aet/backend/goods/batch/${id}`)
}

//查询分类集合 [{id:1,name:'123'}]
export function queryCategorysService(){
    return axios_instance.post('/v1/aet/backend/category/list/query2')
}

//查询skl模板集合 [{id:1,name:'123'}]
export function querySklTplsService(){
    return axios_instance.post('/v1/aet/backend/sku-template/list/query2')
}

export function suppliersService(params){
    return axios_instance.post('/v1/aet/backend/supplier/list',params)
}

//分类列表
export function categorieAddService(params){
    return axios_instance.post('/v1/aet/backend/category/add',params)
}

export function categorieUpdateService(params){
    return axios_instance.post('/v1/aet/backend/category/update',params)
}

export function categoriesService(params){
    return axios_instance.post('/v1/aet/backend/category/list',params)
}

export function queryCategoriesByIdService(pid){
    return axios_instance.post(`/v1/aet/backend/category/${pid}/list`)
}

export function skuTplsService(params){
    return axios_instance.post('/v1/aet/backend/sku-template/list',params)
}

export function updateTplStatusService(params){
    return axios_instance.post('/v1/aet/backend/sku-template/update-status',params)
}

export function queryBrandsService(name){
    return axios_instance.get('/v1/aet/backend/brand/query',{params:{ name }})
}

export function addSkuTplService(params){
    return axios_instance.post('/v1/aet/backend/sku-template/add',params)
}

export function skusService(params){
    return axios_instance.post('/v1/aet/backend/sku/list',params)
}

export function skuInfoService(id){
    return axios_instance.get(`/v1/aet/backend/sku/detail-info/${id}`)
}

//入库出库
export function warehouseInsService(params){
    return axios_instance.post('/v1/aet/backend/inventory/list',params)
}

export function doWarehouseInService(params){
    return axios_instance.post('/v1/aet/backend/inventory/add',params)
}

//入库详情
export function warehouseInDetailService(id){
    return axios_instance.post(`/v1/aet/backend/inventory/detail/${id}`)
}

//供应商新增
export function supplierAllService(){
    return axios_instance.post('/v1/aet/backend/supplier/list/query')
}

export function supplierAddService(params){
    return axios_instance.post('/v1/aet/backend/supplier/add',params)
}

export function goodsSkuTplsService(){
    return axios_instance.post('/v1/aet/backend/goods/list-with-sku')
}

export function carriersAllService(id){
    return axios_instance.post(`/v1/aet/backend/carrier/list/query/${id}`)
}


//生成sku
export function skuAddService(params){
    return axios_instance.post('/v1/aet/backend/sku/add',params)
}

//批量添加sku
export function skuBatchAddService(params){
    return axios_instance.post('/v1/aet/backend/sku/batch/insert',params)
}

//库房
export function wmListService(params){
    return axios_instance.post('/v1/aet/backend/wm/list',params)
}