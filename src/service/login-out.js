import axios_instance from '../axios';

export function loginService(params){
    return axios_instance.post('/api/user/login',params)
}

export function logoutService(params){
    return axios_instance.post('/api/user/logout',params)
}