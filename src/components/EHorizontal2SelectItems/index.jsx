import React from 'react';
import './index.less'
import { Select } from 'antd'  ;

const Option = Select.Option;
export default class EHorizontal2SelectItems extends React.PureComponent{

    static defaultProps = {
        options: []
    }

    handleParentChange = pid =>{
      this.triggerChange({ pid });
    }

    handleSonChange = id =>{
      this.triggerChange({ id });
    }

    triggerChange = (values) => {
      const { onChange } = this.props;
      if (onChange) {
        onChange(values);
      }
    };
      
    render(){
        const { values,options,subOptions,placeholder="请选择" } = this.props;
        return (
            <span className="horizontal-select-box">
                <Select
                    value={values.pid}
                    style={{ width: '55%', marginRight: '3%' }}
                    placeholder={placeholder}
                    onChange={this.handleParentChange}
                    >
                      {
                          options.map(item=><Option key={item.id} value={item.id}>{item.name}</Option>)
                      }
                </Select>
                <Select
                    value={values.id}
                    style={{ width: '42%' }}
                    onChange={this.handleSonChange}
                    >
                      {
                        subOptions.map(item=><Option key={item.id} value={item.id}>{item.name}</Option>)
                      }
                </Select>
            </span>
        );
    }
}