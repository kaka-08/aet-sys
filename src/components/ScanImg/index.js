/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react';
import { Modal } from 'antd';
import './index.less';

const ScanImg =(props)=>{
  const [visible, setVisible] = useState(false);
  const [previewStyle] = useState(props.preview || {width: 450,height:350});

  const modalWidth = previewStyle.width + 50;

  const { src,style = {width: 50,height: 50 } } = props;
  const handleClick = ()=>{
      setVisible(true);
  }

  return (
    <>
      <img onClick={handleClick} src={src} style={{...style}} />
      <Modal
          visible={visible}
          onCancel={()=>setVisible(false)}
          footer={null}
          width={modalWidth}
          >
            <div className="img-scan-modal">
              <img src={src} style={{...previewStyle}}/>
            </div>
      </Modal>
    </>
  );
}

export default ScanImg