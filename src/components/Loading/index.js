import React from 'react';
import { Spin } from 'antd';
import './index.less'

function Loading(){
    return(
        <div className="loading">
            <Spin tip="加载中..."/>
        </div>
    )
}

export default Loading

