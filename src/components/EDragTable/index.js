import React from 'react';
import  { Table,Alert } from 'antd';
import { DndProvider, DragSource, DropTarget } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import update from 'immutability-helper';
import './index.less';

let instanceComponent;
let dragingIndex = -1;

class BodyRow extends React.Component {
  render() {
    const { isOver, connectDragSource, connectDropTarget, moveRow, ...restProps } = this.props;
    const style = { ...restProps.style, cursor: 'move' };

    let { className } = restProps;
    if (isOver) {
      if (restProps.index > dragingIndex) {
        className += ' drop-over-downward';
      }
      if (restProps.index < dragingIndex) {
        className += ' drop-over-upward';
      }
    }

    return connectDragSource(
      connectDropTarget(<tr {...restProps} className={className} style={style} />),
    );
  }
}

const rowSource = {
  beginDrag(props) {
    dragingIndex = props.index;
    return {
      index: props.index,
    };
  },
};

const rowTarget = {
  drop(props, monitor) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return;
    }

    // Time to actually perform the action
    props.moveRow(dragIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex;
  },
};

const DragableBodyRow = DropTarget('row', rowTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
}))(
  DragSource('row', rowSource, connect => ({
    connectDragSource: connect.dragSource(),
  }))(BodyRow),
);

export default class EDragTable extends React.PureComponent{

    constructor(props){
      super(props);
      this.state={
        dataList:[],
        selectedRowKeys: [],
        total:0,
        pageSize:10,
        current:1,
        selectedItem:null,
        loading: false
      }
      instanceComponent = this;
    }

    components = {
      body: {
        row: DragableBodyRow,
      },
    };

    moveRow = (dragIndex, hoverIndex) => {
      const { dataList } = this.state;
      const dragRow = dataList[dragIndex];

      this.setState(
        update(this.state, {
          dataList: {
            $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]],
          },
        }),
      );
    };

    componentDidMount(){
      this.renderListData();
    }

    //调用
    static refresh=(params)=>{
      instanceComponent.renderListData(params);
    }

    /**
     * 选中事件
     */
    onSelectChange = (selectedRowKeys,selectedRows) => {
      const { updateSelectKey } = this.props;
      this.setState({
        selectedRowKeys,
        selectedItem: selectedRows[0]
        },()=>{
          updateSelectKey(selectedRowKeys,selectedRows[0])
        });
    }

    handleRowClick=(e,record,index)=>{
      const { updateSelectKey } = this.props;
      this.setState({
        selectedRowKeys: record.id ? [record.id] : [],
        selectedItem: record
       },()=>{
        updateSelectKey(record.id ? [record.id] : [],record)
     })
    }

    /**
     * 列表初始化数据加载
     */
    renderListData=(params={})=>{
      this.setState({
        loading: true
      })
      const { autoRefresh } = this.props;
      const { current,pageSize } = this.state;
      const { listService} = this.props;
      params["page_no"] = current;
      params['page_size'] = pageSize;
      listService && listService(params).then(res=>{
        this.setState({
          dataList: res.list,
          total: res.total,
          current: res.page_num,
          loading: false
        },()=>{
          if(autoRefresh){
            let reloadIdx = res.list.findIndex(item=>item[autoRefresh.key] == autoRefresh.value);
            if(reloadIdx !== -1){
              setTimeout(()=>{
                this.renderListData(params);
              },5000)
            }
          }
        })
      })
    }

    /**
     * 分页变化
     */
    handlePaginationChange=(pageNo)=>{
      this.setState({
        current: pageNo
      },()=>{
        this.renderListData()
      })
    }

    showTotal=(total)=>{
      return `总共 ${total} 条数据`;
    }

    render(){
        const { columns = [],showRowSelection = false,tip } = this.props;
        const { loading,dataList,total,selectedRowKeys,current,pageSize } = this.state;
        const defaultRowSelection = {
          type:'checkbox',
          selectedRowKeys,
          onChange: this.onSelectChange,
        };
        return(
          <div>
            <h1 className="tip">{tip}</h1>
            {
              showRowSelection && <Alert 
              className="alert-info" 
              message={<span>已选择 <strong>{`${selectedRowKeys.length}`}</strong> 项  服务调用次数总计 <strong>{`${total}`}</strong> 条</span>} 
              type="info" 
              showIcon 
              closeText={<a onClick={this.clearSelectkeys}>清空</a>}
            />
            }
            <DndProvider backend={HTML5Backend}>
              <Table
                rowKey={record => record.id}
                align={'center'}
                rowSelection={showRowSelection ? defaultRowSelection : null}
                loading={loading}
                {...this.props}
                columns={columns}
                dataSource={dataList}
                components={this.components}
                onRow={(record, index) => ({
                  index,
                  moveRow: this.moveRow
                })}
                // onRow={
                //   record => {
                //     return {
                //       onClick: e=>this.handleRowClick(e,record), // 点击行
                //     };
                //   }
                // }
                pagination={{
                  current:current, 
                  onChange:this.handlePaginationChange,
                  pageSize:pageSize,
                  total:total,
                  showTotal: this.showTotal,
                  showQuickJumper: true
                }}
              />
            </DndProvider>
          </div>
        )
    }
}
