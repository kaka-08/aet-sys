import React from 'react';
import  { Table,Alert } from 'antd';
import styles from './index.less';

var instanceComponent;
export default class ETable extends React.PureComponent{

  constructor(props){
    super(props);
    this.state={
      dataList:[],
      selectedRowKeys: [],
      total:0,
      pageSize:10,
      current:1,
      selectedItem:null,
      loading: false
    }
    instanceComponent = this;
  }

    componentDidMount(){
      this.renderListData();
    }

    componentWillUnmount(){
      instanceComponent = null;
    }

    //调用
    static refresh=(params)=>{
      instanceComponent.renderListData(params);
    }

    /**
     * 选中事件
     */
    onSelectChange = (selectedRowKeys,selectedRows) => {
      const { updateSelectKey } = this.props;
      this.setState({
        selectedRowKeys,
        selectedItem: selectedRows[0]
        },()=>{
          updateSelectKey(selectedRowKeys,selectedRows[0])
        });
    }

    handleRowClick=(e,record,index)=>{
      const { updateSelectKey } = this.props;
      this.setState({
        selectedRowKeys: record.id ? [record.id] : [],
        selectedItem: record
       },()=>{
        updateSelectKey(record.id ? [record.id] : [],record)
     })
    }

    /**
     * 列表初始化数据加载
     */
    renderListData=(_pageNo,_pageSize)=>{
      this.setState({
        loading: true
      })
      const { autoRefresh } = this.props;
      const { current,pageSize } = this.state;
      const { listService,pageKey='page',params={},resetState=function(){} } = this.props;
      params[pageKey] = _pageNo || current;
      params['page_size'] = _pageSize || pageSize;
      listService && listService(params).then(res=>{
        this.setState({
          dataList: res.item_list || res.list,
          total: res.total_count,
          current: res.page,
          loading: false
        },()=>{
          resetState()
          if(autoRefresh){
            let reloadIdx = res.item_list.findIndex(item=>item[autoRefresh.key] == autoRefresh.value);
            if(reloadIdx !== -1){
              setTimeout(()=>{
                this.renderListData();
              },5000)
            }
          }
        })
      })
    }

    /**
     * 分页变化
     */
    handlePaginationChange=(pageNo)=>{
        this.renderListData(pageNo)
    }

    showTotal=(total)=>{
      return `总共 ${total} 条数据`;
    }

    render(){
        const { columns = [],showRowSelection = false,tip } = this.props;
        const { loading,dataList,total,selectedRowKeys,current,pageSize } = this.state;
        const rowSelection = {
          type:'checkbox',
          selectedRowKeys,
          onChange: this.onSelectChange,
        };
        return(
          <div>
            <h1 className="tip">{tip}</h1>
            <Alert 
              className="alert-info" 
              message={<span>已选择 <strong>{`${selectedRowKeys.length}`}</strong> 项  服务调用次数总计 <strong>{`${total}`}</strong> 条</span>} 
              type="info" 
              showIcon 
              closeText={<a onClick={this.clearSelectkeys}>清空</a>}
            />
            <Table
              rowKey={record => record.id}
              align={'center'}
              rowSelection={showRowSelection ? rowSelection : null}
              loading={loading}
              {...this.props}
              columns={columns}
              dataSource={dataList}
              onRow={
                record => {
                  return {
                    onClick: e=>this.handleRowClick(e,record), // 点击行
                  };
                }
              }
              pagination={{
                current:current, 
                onChange:this.handlePaginationChange,
                pageSize:pageSize,
                total:total,
                showTotal: this.showTotal,
                showQuickJumper: true
              }}
            />
          </div>
        )
    }
}
