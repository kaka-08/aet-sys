import React from 'react';
import './index.less'
import { Input,Select } from 'antd'  ;

const Option = Select.Option;
export default class EHorizontal2Items extends React.PureComponent{

    static defaultProps = {
        options: []
    }

    handleDateChange = (e) => {
      const data = parseInt(e.target.value || 0, 10);
      if (isNaN(data)) {
        return;
      }
      this.triggerChange({ data:data });
    };
    
    handleUnitChange = unit => {
      this.triggerChange({ unit });
    };
    
    triggerChange = newData => {
      const { onChange, value } = this.props;
      if (onChange) {
        onChange({
          ...value,
          ...newData,
        });
      }
    };

      
    render(){
      const { value,options,placeholder="请填写" } = this.props;
      return (
          <span>
              <Input
                  type="text"
                  value={value.data}
                  onChange={this.handleDateChange}
                  placeholder={placeholder}
                  style={{ width: '62%', marginRight: '3%' }}
              />
              <Select
                  value={value.unit}
                  style={{ width: '35%' }}
                  onChange={this.handleUnitChange}
                  >
                    {
                        options.map(item=><Option key={item[0]} value={item[0]}>{item[1]}</Option>)
                    }
              </Select>
          </span>
      );
    }
}