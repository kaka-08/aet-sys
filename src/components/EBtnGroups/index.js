
import React, { Fragment,Component } from 'react';
import { Button,Tooltip } from 'antd';

export default class EButtonGroups extends Component{

    state={}

    renderTipButtonItem=(item)=>{
        if(item.tip){
            // tip有配置跟随切换变化
            if(item.tip.canToggle){
                return(
                    item.disabled 
                    ?
                    <Tooltip placement={item.tip.placement || "topRight"} title={item.tip.title || ""}>
                        { this.renderButtonItem(item) }
                    </Tooltip>
                    :
                    this.renderButtonItem(item)
                )
            }else{
                return(
                    <Tooltip placement={item.tip.placement || "topRight"} title={item.tip.title || ""}>
                        { this.renderButtonItem(item) }
                    </Tooltip> 
                ) 
            }
        }else{
            return this.renderButtonItem(item)
        }
    }

    renderButtonItem=(item)=>{
        return (
            <Button 
                style={item.disabled ? {marginRight:20} : {marginRight:20}}
                key={item.id}
                type={item.type}
                icon={item.icon} 
                disabled={item.disabled || false}
                onClick={item.handleFn}>
                {item.text}
            </Button>
        )
    }

    // 渲染Button Groups 
    renderBtnList=()=>{
        const {  routerKey,curPageBtn,btnItems,checkedPermission=true  } = this.props;
        if(checkedPermission){
           //如果鉴权
            if(curPageBtn.length === 0  ) return null;
            return btnItems.map(item=>{
                return curPageBtn.indexOf(routerKey+'-'+item.btnKey) > -1 ?
                    this.renderTipButtonItem(item)
                :null
            })
        }else{
            return btnItems.map(item=>{
                return this.renderTipButtonItem(item)
            })
        }
    }

    render(){

        return(
            <Fragment>
                { this.renderBtnList() }
            </Fragment>
        )
    }
}



