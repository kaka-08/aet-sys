import React from 'react'
import './index.less'

export default function DetailFooter(){
    return(
        <footer className="detail-footer">版权所有：AWMC（推荐使用谷歌浏览器以获得更快页面响应速度） 技术支持：awmc团队</footer>
    )
}
