import React from 'react'
import Store from "utils/Store";
import { logoutService } from 'service/login-out';
import DetailHeaderRight from '../DetailHeaderRight';
import DetailHeaderLogo from '../DetailHeaderLogo';
import './index.less'


let timer = null;

 class DetailHeader extends React.Component{
    state={
        username: Store.fetch('username') ? Store.fetch('username') : ''
    }

    toLogin=()=>{
        this.props.history.push('/login');
    }

    logout = () => {
        logoutService().then(res=>{
            Store.clear();
            setTimeout(()=>{
                this.props.history.push('/login');
            },300)
        })
    };

    componentWillUnmount(){
        clearInterval(timer)
    }

    render(){
        return (
            <div className="detail-header" {...this.props}>
                <DetailHeaderLogo />
                <DetailHeaderRight />
            </div>
        );
    }
}

export default DetailHeader
