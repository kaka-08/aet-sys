import React from 'react';
import './index.less'

export default function DetailHeaderLogo(){
    return(
        <a className="logo">
            <img alt="logo" src="https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/aws.png" />
            <h1>仓库管理系统</h1>
        </a>
    )
}