import React from 'react';
import UserDropdown from 'layouts/GlobalHeaderRight/UserDropdown';
import './index.less';

export default class DetailHeaderRight extends React.Component{
    render(){
        return(
            <div className="global-header-right">
                {/* 用户 */}
                <UserDropdown username={"萨达阿斯顿"}/>
            </div>
        )
    }
}