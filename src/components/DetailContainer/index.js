import React from 'react';
import DetailHeader from './components/DetailHeader';
import DetailFooter from './components/DetailFooter';
import './index.less'

function DetailContainer(WrappedComponent) {
    return class extends React.PureComponent {
        render() {
          // 将 input 组件包装在容器中，而不对其进行修改。Good!
          return <>
              <DetailHeader/>
              <div className="detail-container">
                <WrappedComponent {...this.props} />
              </div>
              <DetailFooter />
            </>;
        }
      }
  }

export {
    DetailContainer
}