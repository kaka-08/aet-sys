import React from 'react';
import { Form, Row, Col, Input, Button, Icon,Select,DatePicker } from 'antd';
import { base } from 'config/defaults';
import moment from 'moment';
import './index.less'

const { RangePicker } = DatePicker;
const Option = Select.Option;
class EFilterForm extends React.Component {
    state = {
      expand: false,
    };
  
    // To generate Form.Item
    renderFormItems() {
      const { getFieldDecorator } = this.props.form;
      const { formList } = this.props;
      const children = [];
      for(let i=0,len=formList.length;i<len;i++){
        let item = formList[i];
        let itemDom;
        let tag = item.type.toLocaleLowerCase();
        if(tag === "input"){
          itemDom = (
            <Col key={i+''} span={item.colSpan || base.col}>
              <Form.Item label={item.label}>
                {
                  getFieldDecorator(item.field, {})(
                    <Input placeholder={item.placeholder} onChange={item.onChange} />
                  )
                }
              </Form.Item>
            </Col>
          )
        }
        if(tag === "select"){
          itemDom = (
            <Col key={i+''} span={item.colSpan || base.col}>
              <Form.Item label={item.label}>
                {
                  getFieldDecorator(item.field, {
                    initialValue: item.initialValue || "-1"
                  })(
                    <Select placeholder={item.placeholder}>
                      {
                        item.options.map(_item=><Option style={{fontSize: 12}} key={_item.id} value={_item.id}>{_item.name}</Option>)
                      }
                    </Select>
                  )
                }
              </Form.Item>
            </Col>
          )
        }
        // if(tag === "datepicker"){
        //   itemDom = (
        //     <Col span={item.colSpan}>
        //       <Form.Item label={item.label}>
        //         {getFieldDecorator(item.field, {
        //         })(<DatePicker placeholder={item.placeholder}/>)}
        //       </Form.Item>
        //     </Col>
        //   )
        // }
        children.push(itemDom);
      }
      let colRate = 24 / base.col;
      let colSpan = children.length %colRate>0 ? base.col : 24;
      children.push(
        <Col key={'10'} span={colSpan||base.col} style={{textAlign:colSpan === base.col ?'left':'right'}}>
          <Form.Item>
            <Button type="primary" htmlType="submit">查询</Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>重置</Button>
          </Form.Item>
        </Col>
      )
      return children;
    }
  
    filterSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        this.props.handleFilter(values);
      });
    };
  
    handleReset = () => {
      this.props.form.resetFields();
    };
  
    toggle = () => {
      const { expand } = this.state;
      this.setState({ expand: !expand });
    };
  
    render() {
      return (
        <Form className="ant-advanced-search-form main" onSubmit={this.filterSubmit}>
          <Row gutter={24}>{this.renderFormItems()}</Row>
        </Form>
      );
    }
  }
  
  export default Form.create({})(EFilterForm);
