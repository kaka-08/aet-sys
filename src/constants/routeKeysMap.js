const keysMap = new Map([
    ["",'首页'],
    ["/res",'商品资源'],
    ["/res/brand",'商标'],
    ["/res/goods",'商品'],
    ["/res/category",'品类'],
    ["/res/sku",'sku'],
    ["/res/sku/list",'列表'],
    ["/res/sku/tpl",'模板'],
    ["/res/sku/tpl",'模板'],
    ["/res/sku/tpl-edit",'操作'],
    ["/res/sku/tpl-edit/0",'创建'],
    ["/third-party",'合作伙伴'],
    ["/third-party/supplier",'供应商'],
    ["/third-party/supplier/list",'列表'],
    ["/third-party/supplier/carrier",'承运人'],
    ["/warehouse",'仓库'],
    ["/warehouse/in",'入库'],
    ["/warehouse/out",'出库'],
    ["/warehouse/now",'最新'],
])

export default keysMap


