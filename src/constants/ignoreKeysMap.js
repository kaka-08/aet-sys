const ignoreKeysMap = new Map([
    ["/cicd/deploy",'项目发版详情'],
    ["/workflow/info",'工单流程详情'],
    ["/workflow/do",'审批']
])

export default ignoreKeysMap


