const unit =  new Map([
    [1,"ml"],
    [2,"L"],
    [3,"g"],
    [4,"kg"],
    [5,"箱"],
    [6,"条"],
    [7,"包"],
    [8,"瓶"],
    [9,"盒"],
    [10,"其他"]
]);

const purpose = new Map([
    [1,"食用"],
    [2,"清洁"],
    [3,"医用"],
    [4,"穿着"],
    [5,"装饰"],
    [6,"其他"]
])


const taste = new Map([
    [1,"清真"],
    [2,"微辣"],
    [3,"香辣"],
    [4,"麻辣"],
    [5,"五香"],
    [6,"蒜香"],
    [7,"原味"],
    [8,"其他"]
])

const colors = new Map([
    [1,"红色"],
    [2,"黄色"],
    [3,"绿色"],
    [4,"蓝色"],
    [5,"粉色"],
    [6,"紫色"],
    [7,"白色"],
    [8,"其他"]
])

const expirationUnit = new Map([
    [1,"天"],
    [2,"个月"]
])

const isPromotion = new Map([
    [0,"否"],
    [1,"是"]
])

export {
    unit,
    purpose,
    taste,
    colors,
    expirationUnit,
    isPromotion
}