/**
 * 给定一个整数类型的数组 nums，请编写一个能够返回数组“中心索引”的方法。
 * 我们是这样定义数组中心索引的：数组中心索引的左侧所有元素相加的和等于右侧所有元素相加的和。
 * 如果数组不存在中心索引，那么我们应该返回 -1。如果数组有多个中心索引，那么我们应该返回最靠近左边的那一个
 * slice(start, end) ,其中end不包括自己
 * @param {number[]} nums
 * @return {number}
 */
var pivotIndex = function(nums) {
    // 例如 [1, 7, 3, 6, 5, 6] 

    var result = -1;
    if( nums.length == 0 ){
        result = -1;
    }
    // 1: 数组 第 0 位是 
    // 2: 数组 最后一位是 

    // 3: 数组 中间
    for(let i=0;i<nums.length;i++){
        // 左侧数组
        let leftArr = nums.slice(0,i+1);
        // 右侧数组
        let rightArr = nums.slice(i);

        // 左侧数组和
        let sumLeft = leftArr.reduce(function(a,b){return a+b});
        // 右侧数组和 
        let sumRight = rightArr.reduce(function(a,b){return a+b});

        if(sumLeft === sumRight){
            result = i;
            break;
        }
    }
    return result
};

/**
 * 在一个给定的数组nums中，总是存在一个最大元素 。
 * 查找数组中的最大元素是否至少是数组中每个其他数字的两倍。
 * 如果是，则返回最大元素的索引，否则返回-1。
 */
var dominantIndex = function(nums) {
    // 获取最大元素的索引
    function getMaxValueOfIndex(arr=[]){
        let index = -1;
        let max = 0;
        if(arr.length === 0){
            return { index,max }
        }
        for(let i in arr){
            if(arr[i] > max){
                index = i;
                max = arr[i];
            }
        }
        return index;
    };
    let index = getMaxValueOfIndex(nums);
    // 传入的数组，除了index以外，别的都 *2
    var target = nums.map(function(item,itemIndex){ if(itemIndex != index){ return item*2}else{return item } });
    let resultIndex = getMaxValueOfIndex(target);

    if(target[index] >= target[resultIndex]){
        return Number(index)
    }
    return -1;
};

/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function(digits) {
    //直接 + 1
    // eslint-disable-next-line no-undef
    let tempNum = BigInt(digits.join(''))+1n + ''
    let result = tempNum.split('');
    return result.map(item=>item*1)
};


/**
 * 根据id删除对应元素
 */
function delOptionById(id){
    let options = [
        {
            id: 1,
            name: "张三"
        },
        {
            id: 2,
            name: "李四"
        }
    ];
    let index = options.findIndex(item=>item.id == id);
    if(index !== -1){
        options.splice(index,1)
    }
    return options
}

function doOne(){
    const s = new Date().getSeconds();

    setTimeout(function() {
    // 输出 "2"，表示回调函数并没有在 500 毫秒之后立即执行
    console.log("Ran after " + (new Date().getSeconds() - s) + " seconds");
    }, 500);

    while(true) {
    if(new Date().getSeconds() - s >= 2) {
        console.log("Good, looped for 2 seconds");
        break;
    }
    }
}

function doTwo(){
    console.log('this is two start')
    setTimeout(function(){
        console.log('this is two doing')
    },1000)
    console.log('this is two end')
}


function do2(){
    doTwo();
    doOne();
}


/**
 * 
 * 二分查找，返回target所在的索引
 * @param { array } nums 有序的数组，否则二分查找没有意义
 * @param { number } target 
 */
function binarySearch(nums,target){
    if(!nums || nums.length === 0) return -1;

    let left = 0,right = nums.length - 1; // 左侧0开始，右侧末位开始

    while(left <= right){
        let mid = left + Math.floor((right - left) / 2);
        if(nums[mid] === target){ return mid; }
        else if(nums[mid] < target) { left = mid + 1; }
        else { right = mid - 1; }
    }
    // End Condition: left > right
    return -1;
}


/**
 * 
 * 二分查找，计算数字的平方根
 * @param { number } x 
 */
function binarySearchX(x){
    let left = 1,right = x - 1,ans = 0;
    while(left <= right){
        let mid = left + Math.floor((right - left) / 2);
        if(mid === Math.floor(x / mid)){ return mid; }
        else if(mid < Math.floor(x / mid)) { left = mid + 1; ans = mid }
        else { right = mid - 1; }
    }
    return ans;
}

var search1 = function(nums,target){
    let isAsc = false;  //是否升序
    let left = 0,right = nums.length - 1,ans = -1;
    if(!nums || nums.length === 0) return ans;
    while(left <= right){
        isAsc = nums[left] - nums[right] > 0 ? true : false; 
        let mid = left + Math.floor((right - left) / 2);
        if(nums[mid] === target){ return mid; }
        else if(nums[mid] < target && mid !== nums.length -1 && mid !== 0) {
            if(isAsc){
                left = mid + 1; ans = mid;
            }else{
                right = mid - 1; ans = mid; 
            }
        }
        else { 
            if(isAsc){
                right = mid - 1;
            }else{
                left = mid + 1;
            }
            ans = -1; 
        }
    }
    return ans;
}


var search = function(nums,target){
    function rightVal(current, max){
        return current < max ? current + 10000 - max : current;
    }
    // 难点是 升序被打乱，再制造升序就好了，例如输入为 [4,5,6,7,0,1,2] 8 => [4,5,6,7,8,9,10]
    let left = 0,right = nums.length - 1,ans = -1;
    if(!nums || nums.length === 0) return ans;
    while(left <= right){
        let mid = left + Math.floor((right - left) / 2);
        if(nums[mid] === target){ return mid; }
        else if(rightVal(target, nums[0]) > rightVal(nums[mid], nums[0]) && mid < nums.length - 1) {
            left = mid + 1;;
        }else { 
            right = mid - 1;
        }
    }
    return ans;
}

var solution = function(isBadVersion) {
    /**
     * @param {integer} n Total versions
     * @return {integer} The first bad version
     */
    return function(n) {
        let left = 0,right = n,first = -1;

        while(left < right){
            // 从左往右执行 二分法 , 二分法的第二种模版，当 left === right 的时候 结束 
            let mid = left + Math.floor((right - left) / 2);  // 0 + 2 = 2
            // 如果 n == 30 
            // mid => 15 
            if(isBadVersion(mid)){
                first = mid;
                right = mid;
            }else{
                left = mid + 1;
            }
        };

        if( left !== n && isBadVersion[left] ) return left;

        return first;
    };
};


/**
 * 找出数组当中最小值，并且返回其index
 * @param { array } nums 
 */
var findMin = function(nums) {
    let left = 0,right = nums.length;
    while( left < right ){
        let mid = left + Math.floor((right - left) /2) ; // 二分查找
        if(nums[mid] >= nums[0]){
            left = mid +1;
        }else{
            right = mid;
        }
    }
    
    //循环结束之后
    if(left == nums.length){
        //说明找了一圈，没找到比 0 小的
        return nums[0]
    }
    
    return nums[left]

};


/**
 * 找出数组当中的峰值，并且返回其索引 [ 1,2,3,2,1 ]  => 2 
 * 或者  [1,2,3,2,1,4,1] => 2 或者 6 
 * @param { array } nums 
 */
var findPeakElement = function(nums) {

    let left = 0,right = nums.length - 1;
    while( left < right ){
        let mid = left + Math.floor(( right - left )/2 );
        if( nums[mid] < nums[mid + 1]){
            // 如果当前不处于峰值，则继续 二分查找
            left = mid + 1;
        }else{
            right = mid;
        }
    }
    return left;
};

/**
 * 
 * @param { array } nums 
 * @param { number } target 
 */
function binarySearch1(nums, target) {
    if (!nums || nums.length === 0)
        return -1;

    let left = 0, right = nums.length - 1;
    while (left + 1 < right){
        // Prevent (left + right) overflow
        let mid = left + Math.floor((right - left) / 2);
        if (nums[mid] === target) {
            return mid;
        } else if (nums[mid] < target) {
            left = mid;
        } else {
            right = mid;
        }
    }

    // Post-processing:
    // End Condition: left + 1 == right
    if(nums[left] == target) return left;
    if(nums[right] == target) return right;

    return -1;
}

/**
 * 
 * 给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。
 * 你的算法时间复杂度必须是 O(log n) 级别。
 * 如果数组中不存在目标值，返回 [-1, -1]。
 * @param {*} nums 
 * @param {*} target 
 */
var searchRange = function(nums, target) {

    function doLeftSearch(right,nums,target){
        let left = 0;
        let startIndex = -1;
        if(nums[right] === target){
            startIndex = right; 
        }
       
        while( left + 1 < right ){
            let mid = left + Math.floor(( right - left ) / 2);
            if(nums[mid] === target){
                // 如果左侧有目标值，则继续左行
                right = mid;
                startIndex = mid;
            }else if(nums[mid] < target){
                // 当前值小于目标值，则右行
                left = mid;
            }else{
                // 当前值大于目标值，其实不可能出现此情况
                right = mid;
            }
        }
    
        if(nums[0] === target){
            startIndex = 0;
        }
        return startIndex;
    }

    let result = [];
    let left = 0, right = nums.length -1;
    while(left+1 < right){
        // 这是二分法的第三种模型
        let mid = left + Math.floor( ( right - left ) / 2);
        if(nums[mid] === target){
            // 如果碰巧是结果，则向右侧或者左侧偏移一位，
            result.push(mid);
            left = mid;
        }else if(nums[mid] < target){
            left = mid;   // 向右查找
        }else{
            right = mid; // 向左查找
        }
    }
    //循环结束之后，可能存在一种情况，目标只检索了一半，所以 left/right 的目的就是如此
    if(nums[left] === target && result.indexOf(left) === -1){
        result.push(left)
    }

    if(nums[right] === target && result.indexOf(right) === -1){
        result.push(right)
    }


    let startIndex = doLeftSearch(left,nums,target);

    startIndex !== -1 && result.unshift(startIndex);

    if(result.length > 2){
        return [result[0],result[result.length-1]]
    }

    if(!result.length){
        return [-1,-1]
    }

    if(result.length === 1){
        return [result[0],result[0]]
    }

    return result

};

/**
 * 给定一个排序好的数组，两个整数 k 和 x，从数组中找到最靠近 x（两数之差最小）的 k 个数。返回的结果必须要是按升序排好的。如果有两个数与 x 的差值一样，优先选择数值较小的那个数。
 * @param { number[] } arr 目标数组
 * @param { number } k 返回的数组长度
 * @param { number } x 接近的目标值
 * @return {number[]}
 */
var findClosestElements = function(arr, k, x) {
    // 做题思路： 1 二分法找到最接近元素的值
    let left = 0, right = arr.length -1;

    let minIndex;

    let minNo = 10000; // 最小差

    let tempArr = []; //临时数组

    //循环结束的条件是  left + 1 === right
    while(left + 1 < right){
        let mid = left + Math.floor(( right - left ) / 2 );
        let diff = Math.abs(arr[mid] - x);
        if( diff < minNo ){
            minNo = diff;
        }

        if(arr[mid] === x){
            minIndex = mid;
            left = right -1; // 如果发现有
        }else if(arr[mid] < x){
            left = mid
        }else{
            right = mid
        }
    }

    if(Math.abs(arr[left] - x) < minNo) minNo = Math.abs(arr[left] - x);
    if(Math.abs(arr[right] - x) < minNo) minNo = Math.abs(arr[right] - x);

    console.log('minNo %o',minNo);
};