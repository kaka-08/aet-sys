const { override, fixBabelImports,addLessLoader,addWebpackAlias,addWebpackPlugin } = require('customize-cra');
const CompressionPlugin = require('compression-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');

module.exports = override(
    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: 'true',
    }),
    addLessLoader({
      javascriptEnabled: true,
      modifyVars: {
          'primary-color': '#ff6a00',
          'link-color': '#ff6a00'
        },
    }),
    addWebpackAlias({
      "components": path.resolve(__dirname, 'src/components/'),
      "utils": path.resolve(__dirname, 'src/utils/'),
      "service": path.resolve(__dirname, 'src/service/'),
      "config": path.resolve(__dirname, 'src/config/'),
      "layouts": path.resolve(__dirname, 'src/layouts/'),
      "constants": path.resolve(__dirname, 'src/constants/')
    }),
    addWebpackPlugin(
      // new CompressionPlugin({
      //   filename: '[path].br[query]',
      //   algorithm: 'brotliCompress',
      //   test: /\.(js|css|html|svg)$/,
      //   compressionOptions: { level: 11 },
      //   threshold: 10240,
      //   minRatio: 0.8,
      // }),
      new BundleAnalyzerPlugin(),
      new CompressionPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8,
      })
    )
);
